/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.com/docs/gatsby-config/
 */

module.exports = {
  siteMetadata: {
    title: "Mapachio Hosting Reforzado - Servicio de Hostings y dominios mundiales. Le damos cariño a tu web.",
    defaultDescription:
      `Conoce Mapachio, la nueva era en servicios de Hostings y Dominios. No somos una empresa común y corriente, nos preocupamos por darte las mejores tecnologías para cumplir nuestro objetivo final Darle Cariño a Tu Web`,
    siteUrl: `https://mapachio.com`,
    titleTemplate: "Mapachio - Le Damos Cariño a tu Web - %s",
    url: "https://mapachio.com", // No trailing slash allowed!
    image: "mapachio.png", // Path to your image you placed in the 'static' folder
    twitterUsername: "@occlumency",
  },
  plugins: [
    `gatsby-plugin-emotion`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sitemap`,
    {
      resolve: "gatsby-source-graphql",
      options: {
        // Arbitrary name for the remote schema Query type
        typeName: "WPGraphQL",
        // Field under which the remote schema will be accessible. You'll use this in your Gatsby query
        fieldName: "wpcontent",
        // Url to query from
        url: "https://graphql.mapachio.com/graphql",
      },
    },
    
  ],
}
