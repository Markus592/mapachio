module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      screens: {
        'tab': '920px',
      },
      colors:{
        'azul':'#00003D',
        'rosado':'#FF007C',
        'azulino':'#00008E',
        'gris':'rgb(66,66,66)'
      }
    }, 
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
