import React from "react"
import tw from "twin.macro"
import styled from "@emotion/styled"
import { Link } from "gatsby"
import Logo from "../assets/images/LogoHeader.png"
import { Navigation } from "../data/Navigation"
export const WrapperFooter = styled.div`
  ${tw`
    bg-azul
 `};
  .container {
    ${tw`
        grid
        grid-cols-1
        gap-8
        justify-items-center	

        sm:grid-cols-2
        lg:grid-cols-4
        
        `}
  }
`
export const CardFooter = styled.div`
  ${tw`
    text-white
    text-center
    md:text-left
 `};
  &:nth-of-type(1),
  &:nth-of-type(2) {
    ${tw`
    lg:border-r-2
    lg:border-white 
    lg:pr-16 
 `};
  }
`
export const TitleCard = styled.h3`
  ${tw`
    text-lg
 `};
`
export const ItemFooter = styled.li`
  ${tw`
        list-none 
 `};
`
export const LinkFooter = styled.a`
  ${tw`
    text-gray-300
    font-bold
    hover:text-rosado
    hover:delay-100
 `};
`
export const Img = styled.img`
  ${tw`
    w-full
 `};
`
export const Copy = styled.div`
  ${tw`
        text-white
        text-center
        font-bold
        py-4
 `};
`
const Footer = () => {
  let ano = new Date()
  return (
    <WrapperFooter>
      <div className="container wrapper">
        <Img src={Logo}></Img>
        <CardFooter>
          <TitleCard>Mapa de Sitio</TitleCard>
          {Navigation.map((data, key) => {
            const { name, url } = data
            return (
              <ItemFooter key={key}>
                <LinkFooter as={Link} to={url}>
                  {name}
                </LinkFooter>
              </ItemFooter>
            )
          })}
        </CardFooter>
        <CardFooter>
          <TitleCard> Oficinas Perú</TitleCard>
          <ItemFooter>Email Soporte:</ItemFooter>
          <ItemFooter>
            <LinkFooter href="mailto:soporte@mapachio.com">
              soporte@mapachio.com
            </LinkFooter>
          </ItemFooter>
          <ItemFooter>Email Ventas:</ItemFooter>
          <ItemFooter>
            <LinkFooter href="mailto:soporte@mapachio.com">
              ventas@mapachio.com
            </LinkFooter>
          </ItemFooter>
          <ItemFooter>
            <LinkFooter href="tel:+51933138573">933 138 573</LinkFooter>
          </ItemFooter>
        </CardFooter>
        <CardFooter>
          <TitleCard> Oficinas España</TitleCard>
          {/* <ItemFooter>Email Soporte:</ItemFooter> */}
          {/* <ItemFooter>
            <LinkFooter href="mailto:soporte@mapachio.com">
              soporte@mapachio.com
            </LinkFooter>
          </ItemFooter> */}
          <ItemFooter>Email Ventas:</ItemFooter>
          <ItemFooter>
            <LinkFooter href="mailto:espana@mapachio.com">
              espana@mapachio.com
            </LinkFooter>
          </ItemFooter>
          <ItemFooter>
            <LinkFooter href="tel:+34657628676">+34 657 62 86 76</LinkFooter>
          </ItemFooter>
        </CardFooter>
        <CardFooter>
          <TitleCard> Oficinas UK</TitleCard>
          <ItemFooter>Email Soporte:</ItemFooter>
          <ItemFooter>
            <LinkFooter href="mailto:support@mapachio.com">
              support@mapachio.com
            </LinkFooter>
          </ItemFooter>
          <ItemFooter>Email Ventas:</ItemFooter>
          <ItemFooter>
            <LinkFooter href="mailto:sales@mapachio.com">
              sales@mapachio.com
            </LinkFooter>
          </ItemFooter>
          <ItemFooter>
            <LinkFooter href="tel:+447983091518">+44 7983 091518</LinkFooter>
          </ItemFooter>
        </CardFooter>
        <CardFooter>
          <TitleCard> Oficinas Ecuador</TitleCard>
          <ItemFooter>Email Ventas:</ItemFooter>
          <ItemFooter>
            <LinkFooter href="mailto:ecuador@mapachio.com">
              ecuador@mapachio.com
            </LinkFooter>
          </ItemFooter>
          <ItemFooter>
            <LinkFooter href="tel:+593988382233">+59 398 838 2233</LinkFooter>
          </ItemFooter>
        </CardFooter>
      </div>
      <Copy>
        {" "}
        Copyright {ano.getFullYear()} / Hecho con amor por nuestro gran equipo{" "}
      </Copy>
    </WrapperFooter>
  )
}

export default Footer
