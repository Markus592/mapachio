import tw from 'twin.macro'
 import styled from '@emotion/styled'
 export const CardNosotros = styled.div`
 .container{
  ${tw`max-w-3xl`};
 }
  ${tw`bg-azul py-10 text-center`};
  p{
    ${tw`text-rosado text-3xl md:text-4xl pb-8 font-bold`};
  }
  p.text-white{
    ${tw`text-white font-thin `}; 

  }
  h3{
      ${tw`text-white text-5xl md:text-6xl pb-8`};
  }
 `