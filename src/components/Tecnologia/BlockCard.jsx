import tw from 'twin.macro'
 import styled from '@emotion/styled'
 export const Item = styled.div`
 max-width:1440px;
 
  ${tw` grid gap-8 items-center py-10 mx-auto justify-center`};
  grid-template-columns:auto auto;
  grid-template-areas:'it1 it2';
  p{
  ${tw`max-w-2xl text-xl md:text-2xl`};
  grid-area:it1;
  }
  img{
  ${tw`mx-auto`};
  grid-area:it2;
  }
  @media (max-width:800px){
      grid-template-columns: 1fr;
    grid-template-areas:'it2' 'it1';
    p{
  grid-area:it1;
  }
  img{
  grid-area:it2; 
  }
  }
 `
 export const BlockTech = styled.div`
  ${tw`
  bg-white
    
    py-10
    px-8
    even:bg-azul
  `}; 
  &:nth-child(even) ${Item} p{
        ${tw`text-white`}; 
  }  
  &:nth-child(odd) ${Item} p{
        ${tw`text-azul`}; 
        grid-area:it2;
  }  
  &:nth-child(odd) ${Item} img{
        ${tw`text-white `}; 
        grid-area:it1;
  }  
 `
