import React from "react"
import tw from "twin.macro"
import styled from "@emotion/styled"
import { FaWhatsapp } from "react-icons/fa"
import { FaFacebook } from "react-icons/fa"
import { MensajeWhatsappGeneral } from "../data/variablesConstantes"
const BtnWhatsapp = styled.a`
  ${tw`
    bg-rosado
    p-4
    text-2xl
    fixed
    text-white
    hover:bg-white
    hover:text-rosado
    delay-300
    hover:delay-300
    border-2
    border-rosado
 `};
 .icon{
     /* ${tw`text-rosado`};    */
    }
    right:0; 
    z-index:99999;  
    top:calc(10vh + 190px);  
    border-top-left-radius: 12%;
    border-bottom-left-radius: 12%;
`
const BtnFacebook = styled.a`
  ${tw`
    bg-rosado
    p-4
    text-2xl
    absolute
    text-white
    hover:bg-white
    hover:text-rosado
    delay-300
    hover:delay-300
    border-2
    border-rosado  
 `};
 .icon{
     /* ${tw`text-rosado`};    */
    }    
    right:0; 
    z-index:0;  
    top:calc(10vh + 100px);
    border-top-left-radius: 12%; 
    border-bottom-left-radius: 12%;
`
const Whatsapp =    () => {
  return (
    <>
    <BtnFacebook
      href="https://www.facebook.com/mapachiohost"
      target="_blank"
      rel="noopener noreferrer"
    >           
      <FaFacebook className="icon"></FaFacebook> 
    </BtnFacebook>  
    <BtnWhatsapp  
      href={`${MensajeWhatsappGeneral}`}
      target="_blank"
      rel="noopener noreferrer"
    >
      <FaWhatsapp className="icon"></FaWhatsapp>
    </BtnWhatsapp>
    
    </>
  )
}

export default Whatsapp
