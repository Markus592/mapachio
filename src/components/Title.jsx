import tw, { styled } from "twin.macro"
export const Title = styled.h2(({ rosado }) => [
  tw`
    text-center
    text-azul
    text-3xl
    font-bold 
    mb-4 
    md:mb-8 
    md:text-5xl
    `,
    rosado && tw`text-rosado `,
])
