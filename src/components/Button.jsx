import tw from "twin.macro"
import styled from "@emotion/styled"
// import { Link } from "gatsby"
export const Button = styled.a`
&.btnMore{
   margin: 0;
   width: 75%;
   justify-content: center;
}
 ${tw`
    bg-rosado
    px-4
    py-2
    text-white
    mx-auto
    rounded-lg	
    mt-8 
    text-center
    inline-flex
    border-2
    border-rosado
    transition
    duration-300
    hover:text-rosado
    hover:bg-white
    hover:delay-100
    items-center
    font-bold
 `};
 .icon{
    ${tw`
        mr-2
        text-base
        group-hover:text-rosado
     `}
 }
` 
export const CardButton = styled.div`
  ${tw`text-center`};
`