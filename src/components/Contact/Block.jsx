import React from "react"
import tw from "twin.macro"
import styled from "@emotion/styled"
import { Title } from "../Title"
import { Button } from "../Button"
import { FaWhatsapp } from "react-icons/fa"
const Container = styled.div`
  ${tw`
    py-16
  `};
`
const Grid = styled.div`
  ${tw`
    grid
    gap-8
  `};
  grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
  margin: 0 auto;
  @media (max-width: 1000px) {
    width: 75%;
  }
  @media (max-width: 640px) {
    width: 100%;
    text-align: center;
  }
`
const Item = styled.div`
  ${tw`
     text-azul
     text-xl
 `};
  h3 {
    ${tw`
    font-bold
    text-2xl
 `};
  }
`
const Block = props => {
  return (
    <Container className="container">
      <Title>{props.title || "undefined"}</Title>
      <Grid>
        {props.data
          ? props.data.map((dt, key) => {
              const { NotBtn, title, text, email, Url } = dt
              return (
                <Item Key={key}>
                  <h3>
                    <span dangerouslySetInnerHTML={{ __html: title  }} />
                  </h3>
                  <p>
                    <span dangerouslySetInnerHTML={{ __html: text  }} />
                  </p>
                  <a href={`mailto:${email}`}>
                    <span dangerouslySetInnerHTML={{ __html: email  }} />
                  </a>
                  <br />
                  {NotBtn ? null : (
                    <Button href={Url}>
                      <FaWhatsapp className="icon"></FaWhatsapp> 
                      <span dangerouslySetInnerHTML={{ __html: title  }} />
                    </Button>
                  )}
                </Item>
              )
            })
          : null}
      </Grid>
    </Container>
  )
}

export default Block
