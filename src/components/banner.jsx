import React from "react"
import tw from "twin.macro"
import styled from "@emotion/styled"
import IconArdilla from "../assets/images/iconArdilla.png"
const Container = styled.div`
  ${tw`
    bg-azul
 `};
 &.white{
  ${tw`
    bg-white
 `};

 }
`
const Card = styled.div`
  ${tw`
    flex
    items-end
    justify-center
    font-bold
    py-12
 `};
  &.white {
    h1 {
      ${tw`
            text-azul
        `}
    }
  }
  h1 {
    ${tw`
        text-rosado 
        text-4xl
        sm:text-5xl
        md:text-6xl
    `}
  }
  img {
    width: 50px;
    margin-right: 1rem;
  }
`
const Banner = props => {
  return (
    <Container className={props.white && "white"}>
      <Card className={props.white && "white"}>
        <img src={IconArdilla} alt={props.alt} />
        <h1>{props.title}</h1>
      </Card>
    </Container>
  )
}

export default Banner
