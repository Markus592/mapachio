import React, { useState } from "react"
import { Title } from "../../Title"
import tw from "twin.macro"
import styled from "@emotion/styled"
import { DataPlanesDomain } from "../../../data/dataPaquetes"
import { Button } from "../../Button"
import { MensajeWhatsappGeneral } from "../../../data/variablesConstantes"
import { FaWhatsapp } from "react-icons/fa"
export const TitleDomain = styled.div`
 ${tw`flex text-center items-center mb-8 flex-col sm:flex-row`};
 span{
 ${tw`text-gris sm:mr-4 mr-0 sm:mb-0  mb-4 font-bold text-2xl`};
 }
`
export const InputDomain = styled.input`
  ${tw`
    border-t-2
    border-b-2
    border-rosado
    mx-auto 
    md:w-3/4
    w-full
    md:w-1/2
    text-center
    block
    py-2
    
  `};
  &::placeholder{
    ${tw`
      
      
    `}
  }
`
export const Wrapper = styled.div`
  ${tw`grid
    grid-cols-1
    gap-8
    lg:grid-cols-4
    md:grid-cols-3
    sm:grid-cols-2
  `};
`
export const TitleItemDomain = styled.div`
  ${tw`
    text-rosado
    font-bold
    mr-1
    group-hover:text-white
 `};
`
export const ItemHead = styled.div`
  ${tw`
    pb-2
    group-hover:border-b
    group-hover:border-white
    flex
    justify-between
    text-xl
    flex-wrap
    items-center
 `};
 .checkItem{
   transform: scale(2);
 }
`
    // invisible
    // text-white
    // group-hover:visible
export const PrecioDomain = styled.div`
  ${tw`
    text-lg
 `};
`
export const Domain = styled.div`
  ${tw`
    text-gris
    text-center
    font-bold
    group-hover:text-azul
    py-2 
 `};
`
export const ItemDomain = styled.div`
  ${tw`px-4 
 py-2
 bg-white
 border-2
 cursor-pointer
 border-rosado
 hover:bg-rosado
 hover:delay-100  
 `};
  ${({ active }) => active && tw`bg-rosado`};
  ${TitleItemDomain} {
    ${({ active }) => active && tw`text-white`}
  }
  ${ItemHead} {
    ${({ active }) => active && tw`border-b border-white`}
  }
  ${PrecioDomain} {
    /* ${({ active }) => active && tw`visible`} */
  }
  ${Domain} {
    ${({ active }) => active && tw`text-azul`}
  }
`
export const CardButton = styled.div`
  ${tw`text-center`};
`
const PlanesDomain = () => {
  const [domain, setDomain] = useState([0, ''])
  const ActiveDomain = (indice, domain) => {
    setDomain([indice, domain])
  }
  ;
  return (
    <div className="container wrapper">
      <Title>Y añade tu dominio</Title>
      <InputDomain
        type="text"
        placeholder="Escribe el nombre de tu negocio"
        name="domainName"
        id="domainName"
        />
      <Wrapper>
        {DataPlanesDomain.map((data, key) => {
          const { title, ejemplo } = data
          return (
            <ItemDomain
              active={domain[0] === key + 1 ? true : false}
              onClick={() => ActiveDomain(key + 1, title)}
              className="group"
              key={key}
            >
              <ItemHead>
                {/* <TitleItem>{title}</TitleItem>
                <Precio>{precio}</Precio> */}
              </ItemHead>
              <Domain>{ejemplo}</Domain>
            </ItemDomain>
          )
        })}
      </Wrapper>
      <CardButton>
        <Button
          className="group"
          target="_blanck"
          href={MensajeWhatsappGeneral}
        >
          <FaWhatsapp className="icon"></FaWhatsapp>
          Envianos tu consulta
        </Button>
      </CardButton>
    </div>
  )
}

export default PlanesDomain