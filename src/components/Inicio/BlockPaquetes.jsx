import React, { useState } from "react"
import { Link } from "gatsby"
import { DataPaquetes } from "../../data/dataPaquetes"
import { Title } from "../Title"
import { DataPlanesDomain } from "../../data/dataPaquetes"
import { Cabecera, CabeceraTitle, Item, Body,Select } from "../Tabs"
import {
  WrapperPlanes,
  ItemPlanes,
  LetraBack,
  Info,
  Plan,
  TitleItem,
  Velocidad,
  Precio,
  Text,
  HeadInfo,
} from "./BlockPlanes"
import {
  InputDomain,
  Wrapper,
  ItemDomain,
  ItemHead,
  TitleItemDomain,
  PrecioDomain,
  Domain,
  CardButton,
  TitleDomain,
  
} from "./domain/PlanesDomain"
import { FaWhatsapp, FaEye } from "react-icons/fa"
import { Button } from "../../components/Button"
// const BodyPaquete = styled.div`
//   ${tw`max-w-3xl mx-auto text-gris`};
// `

const BlockPaquetes = () => {
  const [active, setActive] = useState([0, "INICIO"])
  const [planesMapachio, setPlanesMapachio] = useState([0, ""])
  const [domain, setDomain] = useState([0, "", ""])
  const [nameDomain, setNameDomain] = useState("")
  const ActiveDomain = (indice, domain, precio) => {
    setDomain([indice, domain, precio])
  }
  const handleChange = e => {
    setNameDomain(e.target.value)
  }
  // function para seleccionar plan
  const ActivePlan = (indice, plan) => {
    setPlanesMapachio([indice, plan])
  }
  const ClickTab = (index, title) => {
    setActive([index, title])
    setPlanesMapachio([0, ""])
    setDomain([0, "", ""])
    // console.log(index)
  }
  const MensajeWhatsappPaquete = e => {
    e.preventDefault()
    let planPaquete = active[1]
    let planMapachio = planesMapachio[1]
    let planDomain = domain[1]
    let planPrecio = domain[2]
    let TextMessage = `https://api.whatsapp.com/send?phone=+51933138573&text=
    Hola Mapachio deseo +%0D%0A*el Paquete*+%0D%0A${planMapachio} - ${planPrecio}
    +%0D%0A*junto con el dominio*+%0D%0A${nameDomain} y con la extension [${planDomain}]`
    window.open(TextMessage)
    console.log(
      `${planPaquete}${planMapachio}${planDomain}${nameDomain}${planPrecio}`
    )
  }
  return (
    <div className="container wrapper">
      <Title
        dangerouslySetInnerHTML={{
          __html: "¿Qu&eacute; paquete deseas llevar?",
        }}
      ></Title>
      {/* for movile */}
      <Select
        name="selectPaquete"
        onChange={e =>
          ClickTab(
            parseInt(e.target.value.substring(0, 1)),
            e.target.value.substring(1)
          )
        }
        className="selectPaquetes selectStyle container"
      >
        {DataPaquetes.map((data, key) => {
          const { title } = data
          return (
            <option
              key={key}
              value={key + title}
              dangerouslySetInnerHTML={{ __html: title }}
            />
          )
        })}
      </Select>
      <Cabecera>
        {DataPaquetes.map((data, key) => {
          const { title } = data
          return (
            <CabeceraTitle
              key={key}
              active={key === active[0] ? true : false}
              onClick={() => ClickTab(key, title)}
            >
              <span dangerouslySetInnerHTML={{ __html: title }} />
            </CabeceraTitle>

            //   <BodyPaquete active={key === active ? true : false}>
            //     {text}
            //   </BodyPaquete>
          )
        })}
      </Cabecera>

      <Body>
        {DataPaquetes.map((data, key) => {
          const { text } = data
          return (
            <Item as="p" key={key} active={key === active[0] ? true : false}>
              <span dangerouslySetInnerHTML={{ __html: text }} />
              {/* {text} */}
            </Item>
            //   <BodyPaquete active={key === active ? true : false}>
            //     {text}
            //   </BodyPaquete>
          )
        })}
      </Body>
      <Body>
        {DataPaquetes.map((data, key) => {
          const { plan, url } = data
          return (
            <Item key={key} active={key === active[0] ? true : false}>
              {/* <BlockPlanes data={plan}></BlockPlanes> */}
              <div className="wrapper">
                <WrapperPlanes>
                  {plan
                    ? plan.map((data, key) => {
                        let { title, precio, velocidad, letra, textAlt } = data
                        return (
                          <ItemPlanes
                            active={
                              planesMapachio[0] === key + 1 ? true : false
                            }
                            onClick={() => ActivePlan(key + 1, title)}
                            className="group"
                            key={key}
                          >
                            <HeadInfo>
                              <div className="containIcon">
                                <Link to={url}>
                                  <FaEye />
                                </Link>
                              </div>
                              <div>
                                <PrecioDomain className="precio">
                                  {precio} &nbsp; &nbsp;
                                  <input
                                    type="checkbox"
                                    name="Precio"
                                    id="Precio"
                                    onChange={() => {}}
                                    checked={
                                      planesMapachio[0] === key + 1
                                        ? true
                                        : false
                                    }
                                  />
                                </PrecioDomain>
                              </div>
                            </HeadInfo>
                            <LetraBack>{letra}</LetraBack>
                            <Info>
                              <Plan>
                                <TitleItem>
                                  <span
                                    dangerouslySetInnerHTML={{ __html: title }}
                                  />
                                </TitleItem>
                                <Velocidad>
                                  <span
                                    dangerouslySetInnerHTML={{
                                      __html: velocidad,
                                    }}
                                  />
                                </Velocidad>
                              </Plan>
                              <Text>
                                <span
                                  dangerouslySetInnerHTML={{ __html: textAlt }}
                                />
                              </Text>
                            </Info>
                          </ItemPlanes>
                        )
                      })
                    : null}
                </WrapperPlanes>
              </div>

              <div className="wrapper">
                <Title>¿Con que dominio deseas trabajar?</Title>
                <TitleDomain>
                  <span>¿Qu&eacute; dominio estas buscando?</span>
                  <InputDomain
                    onChange={e => handleChange(e)}
                    type="text"
                    placeholder="Escribe el nombre de tu negocio"
                    name="domainName"
                    // id="domainName1"
                  />
                </TitleDomain>
                <Wrapper>
                  {DataPlanesDomain.map((data, key) => {
                    const { title, precio, ejemplo } = data
                    return (
                      <ItemDomain
                        active={domain[0] === key + 1 ? true : false}
                        onClick={() => ActiveDomain(key + 1, title, precio)}
                        className="group"
                        key={key}
                      >
                        <ItemHead>
                          <TitleItemDomain>
                            <span dangerouslySetInnerHTML={{ __html: title }} />
                          </TitleItemDomain>
                          <div>
                            <Precio>
                              <span
                                dangerouslySetInnerHTML={{ __html: precio }}
                              />
                            </Precio>{" "}
                            &nbsp; &nbsp;
                            <input
                              className="checkItem"
                              type="checkbox"
                              onChange={() => {}}
                              checked={domain[0] === key + 1 ? true : false}
                            />
                          </div>
                        </ItemHead>
                        <Domain>
                          <span dangerouslySetInnerHTML={{ __html: ejemplo }} />
                        </Domain>
                      </ItemDomain>
                    )
                  })}
                </Wrapper>
                <CardButton>
                  <Button
                    className="group"
                    // target="_blanck"
                    href="/"
                    onClick={e => MensajeWhatsappPaquete(e)}
                  >
                    <FaWhatsapp className="icon"></FaWhatsapp>
                    Envianos tu consulta
                  </Button>
                </CardButton>
              </div>
            </Item>
          )
        })}
      </Body>
    </div>
  )
}
export default BlockPaquetes
