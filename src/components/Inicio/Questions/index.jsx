import React, { useState } from "react"
import { DataQuestions, Pregunta2 } from "../../../data/DataQuestion"
import { Title } from "../../Title"
import { Button, CardButton } from "../../Button"
import {
  Alternativa,
  Input,
  Label,
  QuestionItem,
  Subtitle,
  Wrapper,
  WrapperAlternativas,
  WrapperAlternativasImage,
  AlternativaImage,
  TitleImage,
} from "./style"
import P1 from '../../../assets/images/P1.png'
import P2 from '../../../assets/images/P2.png'
import P3 from '../../../assets/images/P3.png'
import P4 from '../../../assets/images/P4.png'
import P5 from '../../../assets/images/P5.png'
const Questions = () => {
  // para validar a que imagen le esta haciendo el click
  const [active, setActive] = useState([0, ""])
  // para validar el checked de cada pregunta y poder sacarlo al final
  const [ischeckedPregunta2, setIscheckedPregunta2] = useState([-1, []])
  const [ischeckedPregunta3, setIscheckedPregunta3] = useState([-1, ""])
  const [ischeckedPregunta4, setIscheckedPregunta4] = useState([-1, ""])

  // function pregunta1
  const ActiveImage = (indice, quien) => {
    setActive([indice, quien])
  }
  // function pregunta con alternativa checkox
  const ActiveChecked = (indice, pregunta, e) => {
    let valueAlternativa = e.target.value
    let verificationChecked = e.target.checked
    console.log(e.target.checked)
    console.log(ischeckedPregunta2)
    switch (pregunta) {
      case 2:
        if (verificationChecked) {
          setIscheckedPregunta2([
            indice,
            [...ischeckedPregunta2[1], valueAlternativa],
          ])
        }
        break
      case 3:
        setIscheckedPregunta3([indice, valueAlternativa])
        break
      case 4:
        setIscheckedPregunta4([indice, valueAlternativa])
        break
      default:
        break
    }
    console.log(ischeckedPregunta3)
  }
  const ValidateQuestions = event => {
    event.preventDefault()
    let MensajeError = "Falta llenar datos de las siguientes preguntas:\n"
    // validate error input checkbox
    if (
      active[1] === "" ||
      ischeckedPregunta2[1] === [] ||
      ischeckedPregunta3[1] === "" ||
      ischeckedPregunta4[1] === ""
    ) {
      if (active[1] === "") {
        MensajeError += "- Pregunta 1"
      }
      if (ischeckedPregunta2[1] === []) {
        MensajeError += "\n- Pregunta 2"
      }
      if (ischeckedPregunta3[1] === "") {
        MensajeError += "\n- Pregunta 3"
      }
      if (ischeckedPregunta4[1] === "") {
        MensajeError += "\n- Pregunta 4"
      }
      // Print Message error of input checkbox
      alert(MensajeError)
    } else {
      let Question2 = `${ischeckedPregunta2[1][0]||''},${ischeckedPregunta2[1][1]||''},${ischeckedPregunta2[1][2]||''}`
      let TextMessage = `https://api.whatsapp.com/send?phone=+51933138573
      &text=Hola Mapachio soy una${active[1]},
       que necesita un hosting  ${Question2},
       con respecto a mis correos  ${ischeckedPregunta3[1]}
       y  ${ischeckedPregunta4[1]}`
      window.open(TextMessage)
      // console.log(TelefonoWhatsapp)
      console.log(TextMessage)
    }
  }
  return (
    <Wrapper>
      <div className="container wrapper">
        <Title rosado={true}>¿No sabes que tipo de hosting elegir?</Title>
        <QuestionItem>
          <Subtitle>1.¿Describenos quien eres?</Subtitle>
          <WrapperAlternativasImage>
            <AlternativaImage
              active={active[0] === 1 ? true : false}
              onClick={() => ActiveImage(1, "Persona Natural")}
              className="group"
            >
              <img src={P1} alt="Persona Natural" />
              <TitleImage> Persona Natural</TitleImage>
            </AlternativaImage>
            <AlternativaImage
              active={active[0] === 2 ? true : false}
              onClick={() => ActiveImage(2, "Pyme Pequeña")}
              className="group"
            >
              <img src={P2} alt="Pyme Pequeña" />
              <TitleImage>Pyme Pequeña</TitleImage>
            </AlternativaImage>
            <AlternativaImage
              active={active[0] === 3 ? true : false}
              onClick={() => ActiveImage(3, "Empresa Mediana")}
              className="group"
            >
              <img src={P3} alt="Empresa mediana" />
              <TitleImage>Empresa mediana</TitleImage>
            </AlternativaImage>
            <AlternativaImage
              active={active[0] === 4 ? true : false}
              onClick={() => ActiveImage(4, "Empresa Grande")}
              className="group"
            >
              <img src={P4} alt="Empresa Grande" />
              <TitleImage> Empresa Grande</TitleImage>
            </AlternativaImage>
            <AlternativaImage
              active={active[0] === 5 ? true : false}
              onClick={() => ActiveImage(5, "Organizacion del estado u ONG")}
              className="group"
            >
              <img src={P5} alt="Empresa Grande" />
              {/* <img src={buho} alt="Organizacion del estado u ONG" /> */}
              <TitleImage> Organizaci&oacute;n del estado u ONG</TitleImage>
            </AlternativaImage>
          </WrapperAlternativasImage>
        </QuestionItem>
        <QuestionItem>
          <Subtitle>2. ¿Qu&eacute; hosting deseas?</Subtitle>
          <WrapperAlternativas>
            {Pregunta2[0].alternativas
              ? Pregunta2[0].alternativas.map((dt, key) => {
                  const { pregunta, text,textValue } = dt
                  return (
                    <Alternativa key={key}>
                      <Input
                        type="checkbox"
                        value={textValue}
                        onClick={e => ActiveChecked(key, pregunta, e)}
                        // checked={ischecked === id ? true : false}
                        onChange={() => {}}

                        //  checked={ischeckedPregunta2===key?true:false}
                      ></Input>
                      <Label>{text}</Label>
                    </Alternativa>
                  )
                })
              : null}
          </WrapperAlternativas>
        </QuestionItem>
        {DataQuestions.map((data, key) => {
          const { pregunta, alternativas } = data
          return (
            <QuestionItem key={key}>
              <Subtitle>{pregunta}</Subtitle>
              <WrapperAlternativas>
                {alternativas
                  ? alternativas.map((alt, key) => {
                      const { text, pregunta,textValue } = alt
                      return (
                        <Alternativa key={key}>
                          <Input
                            type="radio"
                            value={textValue}
                            onClick={e => ActiveChecked(key, pregunta, e)}
                            // checked={ischecked === id ? true : false}
                            onChange={() => {}}
                            checked={
                              pregunta === 2
                                ? ischeckedPregunta2[0] === key
                                  ? true
                                  : false
                                : pregunta === 3
                                ? ischeckedPregunta3[0] === key
                                  ? true
                                  : false
                                : pregunta === 4
                                ? ischeckedPregunta4[0] === key
                                  ? true
                                  : false
                                : null
                            }
                            //  checked={ischeckedPregunta2===key?true:false}
                          ></Input>
                          <Label>{text}</Label>
                        </Alternativa>
                      )
                    })
                  : null}
              </WrapperAlternativas>
            </QuestionItem>
          )
        })}
        <CardButton>
          <Button
            id="btn-question"
            onClick={ValidateQuestions}
            href=""
            // target="_blanck"
            // to={MensajeWhatsappGeneral}
          >
            Enviar Consulta
          </Button>
        </CardButton>
      </div>
    </Wrapper>
  )
}

export default Questions
