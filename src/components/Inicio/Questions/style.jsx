import tw from "twin.macro"
import styled from "@emotion/styled"
export const Wrapper = styled.div`
  ${tw`
        bg-azul

 `};
  .container {
    ${tw`
            max-w-6xl
         `}
  }
`
export const WrapperAlternativasImage = styled.div`
  ${tw`
    grid-cols-1
    sm:grid-cols-2
    md:grid-cols-4
    xl:grid-cols-5
    pt-10
    grid
    gap-8
 `};
  img {
    ${tw`
    w-1/4 
    sm:w-2/3
    md:w-full
    
    `}/* position: absolute; */
  }
`
export const TitleImage = styled.div`
  ${tw`
    text-white
    font-bold
    absolute
    text-center
    px-2
 `};
`
// otra forma de pasar propiedades
// export const AlternativaImage = styled.div(({active})=>[
//   tw`
//   border-2
//   border-rosado
//   flex
//   items-center
//   justify-center
//   relative
//   h-48
//   p-4
//   mb-10
//   cursor-pointer
// `,
// active&&tw`bg-white`,

// ]
// )
export const AlternativaImage = styled.div`
  ${tw`
  border-2
  border-rosado
  items-center
  justify-center
  relative
  flex
  p-4
  mb-10
  cursor-pointer 
  last:h-48  
  xl:last:h-auto
  duration-300
  hover:bg-white
  `};
  &:hover ${TitleImage} {
    ${tw`text-azul`}
  }

  ${({ active }) => active && tw`bg-white`};
  ${TitleImage} {
    ${({ active }) => active && tw`text-azul`};
  }
`

export const QuestionItem = styled.div`
  ${tw`
    py-6
 `};
`
export const Subtitle = styled.h3`
  ${tw`
    font-bold
    text-white
    sm:text-lg
    md:text-xl
 `};
`

export const WrapperAlternativas = styled.div`
  ${tw`
    text-gris
 `};
`
export const Input = styled.input`
  transform: scale(1.5);
  ${tw`
      mr-2 
 `};

  &[type="checkbox"]:checked,
  &[type="radio"]:checked {
    box-shadow: 0 0 0 1px #ff007c;
  }
`
export const Label = styled.label`
  ${tw`
    text-gray-300
  `};
`
export const Alternativa = styled.div`
  ${tw`
    flex
    items-center
 `};
`
