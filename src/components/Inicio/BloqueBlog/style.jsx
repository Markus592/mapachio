import tw from "twin.macro"
import styled from "@emotion/styled"
import { Link } from "gatsby"
export const Wrapper = styled.div`
  ${tw`grid
    gap-8
   

    `};
  grid-template-columns: repeat(auto-fill,minmax(min(20rem,100%),1fr));
`
export const Item = styled(Link)`
${tw`
shadow-xl
rounded-lg
md:rounded-none
md:shadow-none
hover:md:rounded-none
hover:md:shadow-none
hover:shadow-2xl
hover:delay-100
`};
  /* &:nth-of-type(1) {
    grid-column: 1 / span 1;
  }
  &:nth-of-type(2) {
    grid-column: 2 / span 1;
  }
  &:nth-of-type(3) {
    grid-column: 3 / span 1;
    grid-row: 1 / span 2;
    .backgroundColor {
      height: 86%;
      img {
        height: 100%;
        object-fit: cover;
      }
    }
  }
  &:nth-of-type(4) {
    grid-column: 1 / span 2;
  } */
  .backgroundColor {
    background-color: #2a2a5a;
  }
  @media (max-width: 920px) {
    &:nth-of-type(1),
    &:nth-of-type(2),
    &:nth-of-type(3),
    &:nth-of-type(4) {
      grid-column: auto;
      grid-row: auto;
    }
    &:nth-of-type(3) {
      .backgroundColor {
        height: auto;;
        img {
          height: 100%;
          object-fit: cover;
        }
      }
    }
  }
`
export const Imagen = styled.img`
  ${tw`w-full relative opacity-50	`};
`
export const TitleItem = styled.h3`
  ${tw`text-azul 
    text-center
    my-4
    md:text-left
    font-bold
    text-2xl
    `};
`
export const Info = styled.div`
  ${tw`text-gris
    text-center
    md:text-left
    leading-5	
    text-lg
    `};
`
export const Autor = styled.p`
  ${tw`capitalize
    border-gris
    border-b-2`};
`
export const Fecha = styled.p`
  ${tw`text-center
    md:text-right


`};
`
