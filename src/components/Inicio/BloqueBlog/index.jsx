import React from "react"
import { Title } from "../../Title"
import { useStaticQuery, graphql } from "gatsby"
import { Autor, Fecha, Imagen, Info, Item, Wrapper, TitleItem } from "./style"
const Blog = () => {
  const { wpcontent } = useStaticQuery(
    graphql`
      query {
        wpcontent {
          blogs(first: 7) {
            nodes {
              uri
              title
              AcfBlog {
                fecha
                nombreDelAutor
              }
              featuredImage {
                node {
                  sourceUrl
                  altText
                }
              }
            }
          }
        }
      }
    `
  )
  return (
    <div className="container wrapper">
      <Title>Art&iacute;culos de Blog</Title>
      <Wrapper>
        {console.log(wpcontent.blogs.nodes)}
        {wpcontent.blogs.nodes.map((data, key) => {
          const {
            uri,
            title,
            AcfBlog: { fecha, nombreDelAutor },
            featuredImage: {
              node: { sourceUrl, altText },
            },
          } = data
          return (
            <Item key={key} to={uri}>
              <div className="backgroundColor">
                <Imagen src={sourceUrl} alt={altText}></Imagen>
              </div>
              <TitleItem>{title}</TitleItem>
              <Info>
                <Autor>{nombreDelAutor}</Autor>
                <Fecha>{fecha}</Fecha>
              </Info>
            </Item>
          )
        })}
      </Wrapper>
      {/* {console.log(site)} */}
      {/* <p>{site.siteMetadata.title}</p> */}
    </div>
  )
}
export default Blog
