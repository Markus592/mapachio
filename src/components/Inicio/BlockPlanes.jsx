import React from "react"
import tw from "twin.macro"
import styled from "@emotion/styled"

export const WrapperPlanes = styled.div`
  ${tw`grid gap-8 justify-center`};
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: 25rem;
  @media (max-width: 800px) {
    grid-template-columns: repeat(2, 1fr);
    grid-template-rows: repeat(2, 22rem);
  }
  @media (max-width: 640px) {
    grid-template-rows: auto;
    grid-template-columns: 1fr;
  }
`
export const TitleItem = styled.h3`
  ${tw`text-3xl font-bold`};
`
export const LetraBack = styled.p`
  ${tw`
  text-center
  text-pink-100
  text-8xl
  md:text-9xl
  hidden
  sm:block
`}
  line-height:.8;
  /* ${tw`
  hover:delay-300
  delay-300
  text-center
  group-hover:text-base
  group-hover:leading-none
  group-hover:delay-300
  group-hover:text-white
  group-hover:static
  group-hover:flex
  group-hover:justify-end

  `}; */
  font-family: "Baloo";
  /* font-size: 12rem; */
  /* @media (max-width: 900px) {
    top: 50%;
  } */
`
export const Plan = styled.div`
  /* ${tw`border-t-2
   border-b-2
   py-2
   border-rosado
   flex
   justify-between
   delay-300
   group-hover:text-white
   group-hover:delay-300
   group-hover:border-white
   `}; */
`
export const HeadInfo = styled.div`
  ${tw`flex justify-between items-center p-4`}
  .containIcon {
    ${tw` bg-rosado p-2 text-white inline-block`}
  }
  #Precio[type="checkbox"] {
    transform: scale(2);
  }
`
export const Precio = styled.span`
  ${tw``}
  ${tw`
    delay-300
    group-hover:text-azul
    text-right
    font-bold
    py-1
 `};
`
export const Text = styled.p`
  ${`color:white;font-size:0px;`}/* ${tw`
    delay-300
    text-azul
    font-bold
    hidden
    text-center
    group-hover:block
    group-hover:delay-300
    group-hover:py-2
 `}; */
`
export const Info = styled.div`
  ${tw`
      relative
      text-center
      p-4
    `};
`
export const ItemPlanes = styled.div`
  ${tw`border-2
        border-rosado
        text-rosado
        relative
        cursor-pointer
    `};
  ${({ active }) => active && tw`bg-rosado`};
  &:hover {
    ${tw`bg-rosado`}
  }
  &:hover ${Text} {
    ${tw`block py-2  text-xl`}
  }
  ${Text} {
    ${({ active }) => active && tw`block py-2  text-xl`};
  }
  &:hover ${LetraBack} {
    font-size: 0px;
    transition: 500ms;
  }
  ${LetraBack} {
    ${({ active }) =>
      active &&
      `
      font-size:0px;
      transition:500ms;
    `};
  }
  &:hover ${Plan} {
    ${tw`
    text-white
    border-white
    `}
  }
  ${Plan} {
    ${({ active }) =>
      active &&
      tw`
    text-white
    border-white
    `}
  }
  &:hover .precio {
    ${tw`
    text-white
    `}
  }
  .precio {
    ${({ active }) =>
      active &&
      tw`
    text-white
    `}
  }
  ${Precio} {
    ${({ active }) =>
      active &&
      tw`
    text-azul   
    `}
  }
  &:hover ${Info} {
    transform: translateY(0);
    transition: 500ms;
  }
  ${Info} {
    ${({ active }) => active && `transform:translateY(0); transition:500ms`}
  }
  &:hover ${HeadInfo} .containIcon {
    ${tw`bg-white  text-rosado delay-300`}
  }
  ${HeadInfo} {
    .containIcon {
      ${({ active }) => active && tw` bg-white  text-rosado delay-300`}
    }
  }

  /* height:300px; */
`

export const Velocidad = styled.div`
  ${tw`
    font-bold
    text-azul
    text-4xl
    `};
`

const BlockPlanes = props => {
  return (
    <div className="container wrapper">
      <WrapperPlanes>
        {props.data
          ? props.data.map((data, key) => {
              let { title, precio, velocidad, letra, textAlt } = data
              return (
                <ItemPlanes className="group" key={key}>
                  <LetraBack>
                    <span dangerouslySetInnerHTML={{ __html: letra }} />
                  </LetraBack>
                  <Info>
                    <Plan>
                      <TitleItem>
                        <span dangerouslySetInnerHTML={{ __html: title }} />
                        </TitleItem>
                      <Velocidad>
                        <span dangerouslySetInnerHTML={{ __html: velocidad }} />
                        </Velocidad>
                    </Plan>
                    <Precio><span dangerouslySetInnerHTML={{ __html: precio}} /></Precio>
                    <Text><span dangerouslySetInnerHTML={{ __html: textAlt}} /></Text>
                  </Info>
                </ItemPlanes>
              )
            })
          : null}
      </WrapperPlanes>
    </div>
  )
}

export default BlockPlanes
