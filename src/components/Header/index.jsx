import React, { useState } from "react"
import { Header } from "./style"
import Hamburguer from "./burguer"
import { Link } from "gatsby"
import Logo from "../../assets/images/LogoHeader.png"
import NavBar from "./nav"
// import { FaFacebook } from "react-icons/fa"
const Index = () => {
  const [open, setOpen] = useState(false)
  return (
    <Header>
      <div className="inside container">
        <Link className="LinkLogo" to="/">
          <img src={Logo} alt="Mapachio" />
        </Link>
        <NavBar open={open} />
        {/* <FaFacebook className='iconDevicePortrait'></FaFacebook> */}
        <Hamburguer open={open} setOpen={setOpen} />
      </div>
    </Header>
  )
}

export default Index
