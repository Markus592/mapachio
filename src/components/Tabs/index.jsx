import tw from "twin.macro"
import styled from "@emotion/styled"
// contain of title tabs
export const Cabecera = styled.div`
  ${tw`flex justify-center items-center
   max-w-4xl mx-auto
    flex-wrap
    hidden
    md:flex
   `};
`
export const Select = styled.select`
  ${tw`block md:hidden`}
`
// label of the data
export const CabeceraTitle = styled.div(({ active,blanco }) => [
  tw` text-gray-500
          border-b-2
          px-4
          py-1
          cursor-pointer
          text-center 
          text-2xl
          mx-8
          mb-4
          uppercase
      `,
  
  //   colors if active
  active && tw`border-rosado delay-100 ease-in-out`,
  blanco && tw`text-white`
])
export const Item = styled.div(({ active,table }) => [
  tw`p-4  hidden sm:p-8`,
  table && tw`px-0`,
  active && tw` block delay-100`,
])
export const Body = styled.div(({table})=>

[
  tw`max-w-5xl mx-auto text-gris text-2xl `,
  table&&tw`max-w-full	`
],




)

