import React from "react"
import Header from "./Header"
import Footer from "./Footer"
import styled from 'styled-components'
import Whatsapp from "./whatsapp"
const Page = styled.div`
  margin-top: 10vh;
`
const Layout = props => {
  return (
    <>
      <Header />
        <Whatsapp></Whatsapp>
      <Page>{props.children}</Page>

      <Footer />
    </>
  )
}
export default Layout
