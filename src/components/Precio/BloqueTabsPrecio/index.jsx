import React, { useState } from "react"
import {  Cabecera, CabeceraTitle, Select } from "../../Tabs"
const Index = props => {
  const [active, setActive] = useState(0)
  const ClickTab = index => {
    setActive(index)
    // console.log(index)
  }
  return (
    <div className="container wrapper">
      <Select className='selectStyle' onChange={(e)=>window.location.href=e.target.value}>
        {props.data.map
          ? props.data.map((dt, key) => {
              const { title, url } = dt
              return (
                <option value={url} selected={key === props.indice ? true : false}>
                      {title}
                </option>
              )
            })
          : null}
      </Select>
      <Cabecera>
        {props.data.map
          ? props.data.map((dt, key) => {
              const { title, url } = dt
              return (
                <CabeceraTitle
                  key={key}
                  // active={key === active ? true : false}
                  active={key === props.indice ? true : false}
                  blanco={true}
                  as={props.as || null}
                  to={url ? url : null}
                  // onClick={() =>  ClickTab(key)}
                >
                  <span dangerouslySetInnerHTML={{ __html: title }} />
                </CabeceraTitle>
              )
            })
          : null}
      </Cabecera>

      {/* <Body table={true}>
        {props.data.map
          ? props.data.map((dt, key) => {
            const { title, details } = dt
            return (
                <Item
                key={key}
                  active={key === active ? true : false}
                  onClick={() => ClickTab(key)}
                  table={true}
                >
                  <TablePrecio data={details}>
                    <Title rosado={true}>{title}</Title>
                  </TablePrecio>
                </Item>
              )
            })
            : null}
      </Body> */}
    </div>
  )
}

export default Index
