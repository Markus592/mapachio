import React, { useEffect } from "react"
import tw from "twin.macro"
import styled from "@emotion/styled"
import { FaWhatsapp } from "react-icons/fa"
import { Button } from "../../Button"
import { Body } from "../../Tabs"
/* overflow-x-auto	 */
export const Table = styled.table`
  ${tw`
        w-full
        text-white
        md:table
        md:overflow-x-hidden
        block
        text-lg
        `};
  tbody.MoreActive {
    tr:nth-of-type(4) {
      display: none;
    }
    tr:nth-of-type(5),
    tr:nth-of-type(6),
    tr:nth-of-type(7),
    tr:nth-of-type(8),
    tr:nth-of-type(9),
    tr:nth-of-type(10),
    tr:nth-of-type(11) {
      display: grid;
      @media (max-width: 640px) {
        display: table-row;
      }
    }
  }
  th,
  td {
    ${tw`
        border-rosado
        border-2
        border-collapse
        text-center
        `};
    padding: 1rem 1.5rem;
  }
  tr:nth-child(2) {
    th:first-child:before {
      content: "descripción";
      color: #ff007c;
    }
  }
  @media (max-width: 640px) {
    overflow: auto;
  }
`
export const Cabecera = styled.th`
  ${tw`font-bold uppercase even:bg-rosado even:text-white flex items-center justify-center flex-col`};
  @media (max-width: 640px) {
    ${tw``};
    display: table-cell;
  }
`
// export const ScrollFixed = styled.div`
//   ${tw`fixed `};

// `
export const Fila = styled.tr`
  ${tw`grid grid-cols-4`}
  &.grid-3 {
    grid-template-columns: repeat(3, minmax(0, 1fr));
  }
  &:nth-of-type(4) {
    display: table-row;
  }
  &:nth-of-type(4) td {
    /* display: block; */
  }
  &:nth-of-type(5),
  &:nth-of-type(6),
  &:nth-of-type(7),
  &:nth-of-type(8),
  &:nth-of-type(9),
  &:nth-of-type(10),
  &:nth-of-type(11) {
    display: none;
  }
  &:nth-last-of-type(1) {
    display: grid;
    @media (max-width: 640px) {
      display: table-row;
    }
  }

  &.fixedScroll.grid-3 {
    grid-template-columns: repeat(3, 1fr);
  }
  &.fixedScroll {
    position: sticky;
    top: 10vh;
    left: 0;

    /* width:100%; */
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    justify-content: center;
    th {
      ${tw`
        bg-rosado 
        border-white
        w-auto
        duration-300
        text-base
        md:text-xl
        hover:bg-white
        hover:border-rosado
        hover:text-white
        animate-bounce	
        `}
      /* padding:5rem !important; */
      line-height: 1.2;
      animation-name: example;
      animation-duration: 3s;
    }
  }
  &:nth-last-of-type(1) td {
    ${tw`
  border-0
  
  `};
  }
  @media (max-width: 640px) {
    display: table-row;
    &.fixedScroll {
      position: fixed;
      display: table-row;
      ${tw`flex w-full justify-center `}

      th:first-child {
        display: none;
      }
    }
  }
  /* @media (min-width:1280px){
    &.fixedScroll{
      width:auto;
      top:50%;
      right:0;
      left:auto;
      flex-direction: column;
      font-size: inherit;
      th{
        width:10rem;
      }
    }
  } */
`
export const Content = styled.td`
  li {
    ${tw`text-left`};
  }
`
const Index = props => {
  useEffect(() => {
    let Table = document.querySelector(".table tbody")
    window.addEventListener("scroll", () => {
      if (window.scrollY > 500) {
        Table.firstChild.classList.add("fixedScroll")
      } else {
        Table.firstChild.classList.remove("fixedScroll")
      }
    })
  }, [])
  return (
    <Body>
      <div className="container table">
        <Table>
          <tbody>
            {props.data
              ? props.data.map((dt, key) => {
                  const { fila } = dt
                  return (
                    <Fila
                      className=""
                      key={key}
                      className={props.grid3 && "grid-3"}
                    >
                      {fila.map((data, key) => {
                        const {
                          text,
                          Head,
                          colspan,
                          Btn,
                          url,
                          precio,
                          More,
                        } = data
                        return Head ? (
                          <>
                            <Cabecera key={key}>
                              <span
                                dangerouslySetInnerHTML={{ __html: text }}
                              />
                              <span
                                dangerouslySetInnerHTML={{
                                  __html: precio || "",
                                }}
                              />
                            </Cabecera>
                            {/* <ScrollFixed className='scrollFixed' key={key+100}>
                              {text} <br />
                              {precio || ""}
                            </ScrollFixed> */}
                          </>
                        ) : (
                          <Content
                            key={key}
                            colSpan={More && (props.colspan || "4")}
                          >
                            {Btn ? (
                              <Button className="" target="_blanck" href={url}>
                                <FaWhatsapp className="icon"></FaWhatsapp>
                                <span
                                  dangerouslySetInnerHTML={{ __html: text }}
                                />{" "}
                                <br />
                              </Button>
                            ) : // colocando el boton con su respectivo estilo
                            More ? (
                              <Button
                                onClick={e => {
                                  e.preventDefault()
                                  let moreActived = document.querySelector(
                                    "tbody"
                                  )
                                  moreActived.classList.add("MoreActive")
                                  // console.log(moreActived)
                                }}
                                className="btnMore"
                                href="/"
                              >
                                VER MÁS
                              </Button>
                            ) : (
                              <span
                                dangerouslySetInnerHTML={{ __html: text }}
                              ></span>
                            )}
                          </Content>
                        )
                      })}
                    </Fila>
                  )
                })
              : null}
          </tbody>
        </Table>
      </div>
    </Body>
  )
}

export default Index
