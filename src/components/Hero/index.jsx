import React from "react"
import HeroSlider, { Slide, SideNav } from "hero-slider"
// import Ardilla from "../../assets/images/ardilla.png"
// import Buho from "../../assets/images/buho.png"
// import Liebre from "../../assets/images/liebre.png"
// import Mapache from "../../assets/images/mapache.png"
// import MapacheGordo from "../../assets/images/mapacheGordo.png"
import { DataHeroSlider } from "../../data/HeroSlider"
import tw, { styled } from "twin.macro"
import { Link } from "gatsby"
import { Button } from "../Button"
import { FaWhatsapp } from "react-icons/fa"
import Pag1 from "../../assets/images/corazon.png"
import Pag2 from "../../assets/images/email.png"
import Pag3 from "../../assets/images/rayo.png"
import Pag4 from "../../assets/images/candado.png"
import Pag5 from "../../assets/images/audifono.png"
import Pag6 from "../../assets/images/energy.png"
// export const Comilla = styled.span`
//   ${tw`text-rosado`};
// `
const ContentHero = styled.div`
  position: relative;
  ${tw`text-center md:mx-0 mx-auto md:text-left`}
  h1 {
    ${tw`text-white 
        text-5xl
        font-bold
        mb-8
        
        md:text-6xl 
    `};
    text-shadow: 1px 1px 2px black;
  }
  p {
    ${tw`
      text-gray-300
      text-3xl
      max-w-xl
  `}
    text-shadow: 1px 1px 2px black;
  }
`
const Container = styled.div`
  ${tw`h-full relative flex  
        items-center `};
    @media (max-width: 640px)  {
      align-items: start;
    }
    @media (max-height: 640px)  {
      align-items: start;
    }
  img {
    position: absolute;
    right: 0;
    width: auto;
    height: 55vh;
    z-index: -1;
    &.hiddenImgDesktop {
      display: none;
    }
    @media (max-width: 768px) {
      /* top:0; */
      position: static;
      height: 14vh;
      margin: 1.2rem auto 2rem;
      &.hiddenImgDesktop {
        display: block;
      }
      &.hiddenImg {
        display: none;
      }
    }
  }
`
// para cambios de css personalizados
const Wrapper = styled.div`
  ${tw`bg-azul `};
  & > div {
    ${tw`relative mx-auto`};
    max-width: 1440px;
  }
  ul {
    top: 75% !important;
    left: 4vw !important;
    display: flex;
    width: 50% !important;
    /* margin: 2rem 0; */
    /* margin:0 auto !important; */
    @media (max-width: 768px) {
      top: 88% !important;
      width: 100% !important;
      justify-content: center;
      left: 0 !important;
    }
    @media (max-width: 640px) {
      top: 100%;
    }
  }
  .slide-side-nav-button.slide-side-nav-active-button {
    color: #ff007c !important;
    background-color: #ff007c;
    background-size: 50%;
    /* padding: 1rem !important ; */
    .slide-side-nav-button-number {
      margin: 0 !important ;
      visibility: hidden;
    }
  }
  .slide-side-nav-button-line {
    display: none;
  }
  .slide-side-nav-button-number {
    width: 100% !important;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    transform: none !important;
    /* ${tw` border-rosado border-2`} */
    /* width:20px !important; */
  }
  .slide-side-nav-button {
    padding:0 !important;
    display: block !important;
    width: 50px !important;
    height: 50px;
    margin: 0 16px !important;
    text-align: center !important;
    ${tw`border-2 border-rosado  bg-no-repeat bg-center`}
    margin-bottom:.5rem !important;
    background-size: 0 0;
    &:nth-child(1) {
      background-image: url(${Pag1});
    }
    &:nth-child(2) {
      background-image: url(${Pag2});
    }
    &:nth-child(3) {
      background-image: url(${Pag3});
    }
    &:nth-child(4) {
      background-image: url(${Pag4});
    }
    &:nth-child(5) {
      background-image: url(${Pag6});
    }
    &:nth-child(6) {
      background-image: url(${Pag5});
    }
  }
  @media (max-width:500px){
    .slide-side-nav-button {
      margin: 0  !important;
    }
  }
`
const Hero = () => {
  return (
    <Wrapper>
      <HeroSlider
        className="container"
        slidingAnimation="top_to_bottom"
        orientation="horizontal"
        initialSlide={1}
        onBeforeChange={(previousSlide, nextSlide) =>
          console.log("onBeforeChange", previousSlide, nextSlide)
        }
        onChange={nextSlide => console.log("onChange", nextSlide)}
        onAfterChange={nextSlide => console.log("onAfterChange", nextSlide)}
        style={
          {
            // backgroundColor: "#00003D",
          }
        }
        settings={{
          slidingDuration: 600,
          slidingDelay: 100,
          shouldAutoplay: false,
          shouldDisplayButtons: false,
          autoplayDuration: 8000,
          height: "90vh",
        }}
      >
        {DataHeroSlider.map((data, key) => {
          const { title, text, Url, TextLink, img, WhatLink } = data
          return (
            <Slide
              key={key}
              background={{
                // backgroundImage: img,
                backgroundSize: "30%",
                backgroundRepeat: "no-repeat",
                backgroundPosition: "center right",
              }}
            >
              <Container className="container">
                <ContentHero>
                  <img className="hiddenImgDesktop" src={img} alt={title} />
                  <h1 dangerouslySetInnerHTML={{ __html: title }} />
                  <p dangerouslySetInnerHTML={{ __html: text }} />

                  {WhatLink ? (
                    <Button target="_blanck" href={Url}>
                      <FaWhatsapp className="icon"></FaWhatsapp>
                      <span dangerouslySetInnerHTML={{ __html: TextLink }} />
                    </Button>
                  ) : (
                    <Button as={Link} to={Url} href="">
                      <span dangerouslySetInnerHTML={{ __html: TextLink }} />
                    </Button>
                  )}

                  {/* <div dangerouslySetInnerHTML={{ __html: title }} /> */}
                </ContentHero>
                <img className="hiddenImg" src={img} alt={title} />
              </Container>
            </Slide>
          )
        })}
        <SideNav
          position={{
            top: 0,
            right: 0,
          }}
        />
      </HeroSlider>
    </Wrapper>
  )
}

export default Hero
