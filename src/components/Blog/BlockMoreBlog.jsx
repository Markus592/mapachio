import React from "react"
import { Title } from "../Title"
import tw from "twin.macro"
import styled from "@emotion/styled"
import { Link } from "gatsby"
import { useStaticQuery, graphql } from "gatsby"
import { Info } from "../inicio/BloqueBlog/style"
/* sm:grid-cols-2 */
const Imagen = styled.img`
  ${tw`mx-auto  p-4`};
`
export const TitleItem = styled.h3`
  ${tw`text-gris
    text-center
    mb-4
    md:text-left
    font-bold
    text-xl 
    `};
`
export const Wrapper = styled.div`
  ${tw`grid
   grid-cols-1
   md:grid-cols-1
   gap-8
   `};
`
export const Fecha = styled.p`
  ${tw`text-azul text-center
    md:text-left


`};
`
export const Item = styled(Link)`
  ${tw`
shadow-xl
grid
gap-4
items-center
rounded-lg
mx-auto
w-full
xl:w-60
md:rounded-none
md:shadow-none
justify-self-end
hover:md:rounded-none
hover:md:shadow-none
hover:shadow-2xl
hover:delay-100
`};
  @media (max-width: 1000px) {
    grid-template-columns:  200px auto;
  }
  @media (max-width: 500px) {
    grid-template-columns:  1fr;
  }
  .backgroundColor {
    background-color: #2a2a5a;
  }
`
const CardInfo = styled.div`
  ${tw``};
`
const BlockMoreBlog = () => {
  const { wpcontent } = useStaticQuery(
    graphql`
      query {
        wpcontent {
          blogs(first: 5) {
            nodes {
              uri
              title
              AcfBlog {
                fecha
                nombreDelAutor
              }
              featuredImage {
                node {
                  sourceUrl
                  altText
                }
              }
            }
          }
        }
      }
    `
  )
  return (
    <div className="">
      <Title>Ultimos post</Title>
      <Wrapper>
        {console.log(wpcontent.blogs.nodes)}
        {wpcontent.blogs.nodes.map((data, key) => {
          const {
            uri,
            title,
            AcfBlog: { fecha },
            featuredImage: {
              node: { sourceUrl, altText },
            },
          } = data
          return (
            <Item key={key} to={uri}>
              <div className="backgroundColor">
                <Imagen src={sourceUrl} alt={altText}></Imagen>
              </div>
              <CardInfo>
                <TitleItem>{title}</TitleItem>
                <Info>
                  <Fecha>{fecha}</Fecha>
                </Info>
              </CardInfo>
            </Item>
          )
        })}
      </Wrapper>
      {/* {console.log(site)} */}
      {/* <p>{site.siteMetadata.title}</p> */}
    </div>
  )
}
export default BlockMoreBlog
