import React from "react"
// import { Title } from "../Title"
import tw from "twin.macro"
import styled from "@emotion/styled"
import { Link } from "gatsby"
import { useStaticQuery, graphql } from "gatsby"
import {
  Autor,
  Fecha,
  Imagen,
  Info,
  TitleItem,
} from "../inicio/BloqueBlog/style"
export const Wrapper = styled.div`
  ${tw`grid
   gap-8
   sm:grid-cols-2
   md:grid-cols-3
   `};
`
/* md:rounded-none */
/* md:shadow-none */
// hover:md:rounded-none
// hover:md:shadow-none
export const Item = styled(Link)`
  ${tw`
shadow-xl
rounded-lg
hover:shadow-2xl
hover:delay-100
`};
.backgroundColor {
    background-color: #2a2a5a;
  }
  .InfoCard{
    padding:0 1rem;
  }
`
const BlockBlogCard = () => {
  const { wpcontent } = useStaticQuery(
    graphql`
      query {
        wpcontent {
          blogs {
            nodes {
              uri
              title
              AcfBlog {
                fecha
                nombreDelAutor
              }
              featuredImage {
                node {
                  sourceUrl
                  altText
                }
              }
            }
          }
        }
      }
    `
  )
  return (
    <div className="container wrapper">
      
      <Wrapper>
        {console.log(wpcontent.blogs.nodes)}
        {wpcontent.blogs.nodes.map((data, key) => {
          const {
            uri,
            title,
            AcfBlog: { fecha, nombreDelAutor },
            featuredImage: {
              node: { sourceUrl, altText },
            },
          } = data
          return (
            <Item key={key} to={uri}>
              <div className="backgroundColor">
                <Imagen src={sourceUrl} alt={altText}></Imagen>
              </div>
              <div className="InfoCard">
                <TitleItem>{title}</TitleItem>
                <Info>
                  <Autor>{nombreDelAutor}</Autor>
                  <Fecha>{fecha}</Fecha>
                </Info>
              </div>
            </Item>
          )
        })}
      </Wrapper>
      {/* {console.log(site)} */}
      {/* <p>{site.siteMetadata.title}</p> */}
    </div>
  )
}
export default BlockBlogCard
