import React from "react"
import Layout from "../components/Layout"
import tw from "twin.macro"
import styled from "@emotion/styled"
import BlockMoreBlog from "../components/Blog/BlockMoreBlog"
// import {FaWhatsapp} from 'react-icons/fa'
import Whatsapp from "../assets/images/whatsapp.svg"
import SEO from "../components/Seo"
const Wrapper = styled.div`
  ${tw` grid gap-8`}
  grid-template-columns:auto 1fr;
  @media (max-width: 1000px) {
    grid-template-columns: 1fr;
  }
  .WrapperMoreBlog {
    margin-top: 2rem;
  }
`
const Title = styled.h1`
  ${tw`
    text-white
    text-2xl
    md:text-4xl
    sm:text-3xl
  `};
`
const Resumen = styled.p`
  ${tw`
    text-rosado
    text-lg
    sm:text-2xl
    md:text-3xl
    pt-4
    `};
`
const Header = styled.div`
  ${tw`bg-azul
    py-16
    font-bold
    `};
  .container {
    ${tw`grid gap-8 items-center`}
    grid-template-columns:1fr auto;
    @media (max-width: 500px) {
      grid-template-columns: 1fr;
    }
    .info {
      width: calc(100% - 12rem);
      @media (max-width: 768px) {
        width: 100%;
        text-align: center;
      }
    }
    .cardAutor {
      text-align: center;
    }
  }
`
const Body = styled.div`
  ${tw`max-w-3xl`};
  
  figure{
    width:100% !important;
    img{
    margin: 0 auto;
  }
  }
  a{
    padding: 0rem 0.2rem;
border-radius: 0px;
font-weight: bold;
text-decoration: underline;
  }
  h1,
  h2,
  h3,
  h4,
  h5,
  h6{
    margin: 1.5rem 0 0.5rem;
  }
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  strong {
    color: #ff007c;
    font-weight: bold;
  }
  h1 {
    font-size: 2.2em;
  }

  h2 {
    font-size: 1.8em;
  }

  h3 {
    font-size: 1.4em;
  }

  h4 {
    font-size: 1.2em;
  }

  h5 {
    font-size: 1.1em;
  }
  p {
    margin-bottom: 1.5rem;
  }
  .aligncenter {
    text-align: center;
    margin: 0.5rem auto;
  }
  .alignright {
    float: right;
    margin: 0.5rem;
  }

  ul {
    padding-left: 1.2rem;

    margin-bottom:1.5rem;
  }
  li {
    list-style: outside;
  }
  .whatsapp-button {
    background-color: #25d366;
    padding: 1rem;
    display: grid;
    grid-template-columns: auto 1fr;
    border-radius: 20px;
    color: white;
    align-items: center;
    /* font-weight: bold; */
    margin-top: 1rem;
    &::before {
      content: "";
      display: block;
      background-image: url(${Whatsapp});
      background-repeat: no-repeat;
      background-size: cover;
      width: 50px;
      height: 50px;
    }
  }
`
const Autor = styled.div`
  ${tw`
    border-rosado
    border-4
    text-center 
    relative
    inset-0
    inline-block
  `};
`
const ImagenAutor = styled.img`
  ${tw`
    rounded-full
    w-32
    h-32
    m-4 
    self-start
  `}
`
const NombreAutor = styled.p`
  ${tw`text-rosado
  font-bold 
  text-xl 
 `};
`
const FechaAutor = styled.div`
  ${tw`
    text-gris
    font-bold
    pb-4
  `};
`

const Post = ({ pageContext }) => {
  const post = pageContext.page
  return (
    <Layout>
      <SEO
        noTemplate={true}
        detailsBlog={true}
        title={"" + post.title}
        description={post.AcfBlog.textoResumen}
        image={post.featuredImage.node.sourceUrl}
      ></SEO>
      {/* {
        console.log(post.featuredImage)
      } */}
      <Header>
        <div className="container">
          <div className="info">
            <Title>{post.title}</Title>
            <Resumen>{post.AcfBlog.textoResumen}</Resumen>
          </div>
          <div className="cardAutor">
            <Autor>
              <ImagenAutor src={post.AcfBlog.imagenDelAutor.sourceUrl} />
              <NombreAutor>{post.AcfBlog.nombreDelAutor}</NombreAutor>
              <FechaAutor>{post.AcfBlog.fecha}</FechaAutor>
            </Autor>
          </div>
        </div>
      </Header>
      <Wrapper className="container wrapper">
        <Body dangerouslySetInnerHTML={{ __html: post.content }} />
        <div className="WrapperMoreBlog">
          <BlockMoreBlog></BlockMoreBlog>
        </div>
      </Wrapper>

      {console.log(post)}
    </Layout>
  )
}

export default Post
