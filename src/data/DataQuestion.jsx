export const DataQuestions = [
  
  {
    pregunta: "3. ¿Y con respecto a tus correos electrónicos?",
    alternativas: [
      {pregunta:3,textValue:'quiero tener correos electrónicos con mi dominio',text:"Quiero tener correos electrónicos con mi dominio"},
      {pregunta:3,textValue:'deseo manejarlos a través de GMAIL u otras opciones extrenas',text:"Deseo manejarlos a través de GMAIL u otras opciones extrenas"},
      {pregunta:3,textValue:'no deseo correos corporativos',text:"No deseo correos corporativos"},
    ],
  },

  {
    pregunta: "4. ¿Quieres un dominio?",
    alternativas: [
      {pregunta:4,textValue:"quiero un dominio",text:"Quiero un dominio"},
      {pregunta:4,textValue:"ya tengo un dominio",text:"Ya tengo un dominio"},
      {pregunta:4,textValue:"ya tengo un dominio pero por todos los dioses quiero migrarlo con ustedes",text:"Ya tengo un dominio pero por todos los dioses quiero migrarlo con ustedes"},
    ],
  },
]
export const Pregunta2=[
  {
    pregunta: "2. ¿Qué hosting deseas?",
    alternativas: [
      {pregunta:2,textValue:`para subir mi página web`,text:"Deseo un hosting para subir mi página web"},
      {pregunta:2,textValue:`para almacenar videos y contenido multimedia`,text:"Deseo un hosting para almacenar videos y contenido multimedia"},
      {pregunta:2,textValue:`y para tener mi aplicación web`,text:"Deseo un hosting para tener mi aplicación web"},
    ]
  },

]