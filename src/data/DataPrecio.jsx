import { TelefonoWhatsapp } from "./variablesConstantes"
import {
  DataPaquetes,
  DataPlanInicio,
  DataPlanGestor,
  DataPlanEmpresa,
  DataPlanCorporativo,
  DataPlanVPS,
  DataPlanEducacion,
  DataPlanDesarrollo,
} from "./dataPaquetes"
const Mensaje = (typeEnterprise, typePlan) => {
  let MensajeWhatsappPrecios = `https://api.whatsapp.com/send?phone=${TelefonoWhatsapp}&text=Hola Mapachio me gustaria comprar el plan ${typeEnterprise}(${typePlan})`
  return MensajeWhatsappPrecios
}
export const DataPrecioInicio = [
  {
    fila: [
      { text: "PAQUETE", Head: true },
      {
        text: DataPlanInicio[0].title,
        precio: DataPlanInicio[0].precio,
        Head: true,
      },
      {
        text: DataPlanInicio[1].title,
        precio: DataPlanInicio[1].precio,
        Head: true,
      },
      {
        text: DataPlanInicio[2].title,
        precio: DataPlanInicio[2].precio,
        Head: true,
      },
      // { text: DataPlanInicio[3].title, Head: true },
      // { text: DataPlanInicio[4].title, Head: true },
      // { text: DataPlanInicio[5].title, Head: true },
    ],
  },
  {
    fila: [
      { text: "", Head: true },
      { text: DataPlanInicio[0].text, Head: false },
      {
        text: DataPlanInicio[1].text,
        Head: false,
      },
      { text: DataPlanInicio[2].text, Head: false },
    ],
  },
  {
    fila: [
      { text: "Servidor", Head: true },
      {
        text: `
      <li>250MB de Espacio NVMe</li>
      <li>2.5 GB Transferencia</li>
      <li>1 parking dominio</li>
      <li>Sin Subdominio</li>
      <li>Sin BD</li>
      <li>Lenguaje PHP</li>`,
        Head: false,
      },
      {
        text: `
      <li>500MB de Espacio NVMe</li>
      <li>5 GB Transferencia</li>
      <li>1 parking  dominio</li>
      <li>1 parking  subdominio</li>
      <li>1 BD</li>
      <li>Softaculous: Instale Wordpress, Moodle, Joomla, Drupal y otras 300 aplicaciones </li>
      <li>Lenguaje PHP</li>
      `,
        Head: false,
      },
      {
        text: `
      <li>1GB  de Espacio NVMe</li>
      <li>10 GB Transferencia</li>
      <li>1 parking  dominio</li>
      <li>1 parking  subdominio</li>
      <li>1 BD</li>
      <li>Softaculous: Instale Wordpress, Moodle, Joomla, Drupal y otras 300 aplicaciones con un solo click.</li>
      <li>Lenguaje PHP</li>
      
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "", Head: false, More:true},
     
    ],
  },
  {
    fila: [
      { text: "OPTIMIZACI&oacute;N", Head: true },
      {
        text: `
      <li>100% + Velocidad Servidor Web (LiteSpeed).</li>
      <li>Discos NVME (3600% m&aacute;s veloz vs Hosting Normales)</li>
      `,
        Head: false,
      },
      {
        text: `
      <li>100% + Velocidad Servidor Web (LiteSpeed).</li>
      <li>Discos NVME (3600% m&aacute;s veloz vs Hosting Normales)</li>
      
      `,
        Head: false,
      },
      {
        text: `
      <li>100% + Velocidad Servidor Web (LiteSpeed).</li>
      <li>LS Cache: Mejora m&aacute;s la velocidad de tu web en Wordpress al 100% gratis.</li>
      <li>Discos NVME (3600% m&aacute;s veloz vs Hosting Normales)</li>
      
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "CORREOS", Head: true },
      {
        text: `
      <li>3 correos</li>
      <li>RoundCube</li>
      <li>Envi&oacute; y Recepci&oacute;n Proactiva de Mail: Servicio Mejorado de Envi&oacute;</li>
      `,
        Head: false,
      },
      {
        text: `
      <li>5 correos</li>
      <li>RoundCube</li>
      <li>Envi&oacute; y Recepci&oacute;n Proactiva de Mail: Servicio Mejorado de Envi&oacute;</li>
      `,
        Head: false,
      },
      {
        text: `
      <li>10 correos</li>
      <li>RoundCube</li>
      <li>Envi&oacute; y Recepci&oacute;n Proactiva de Mail: Servicio Mejorado de Envi&oacute;</li>
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "SEGURIDAD", Head: true },
      {
        text: `
      <li>Antivirus</li>
      <li>Firewall</li>
      <li>Protecci&oacute;n contra Ataques DDOS</li>
      <li>Seguridad por Reglas.</li>
      
      `,
        Head: false,
      },
      {
        text: `
      <li>Antivirus</li>
      <li>Firewall</li>
      <li>Protecci&oacute;n contra Ataques DDOS</li>
      <li>Seguridad por Reglas.</li>
      `,
        Head: false,
      },
      {
        text: `
      <li>Antivirus</li>
      <li>Firewall</li>
      <li>Protecci&oacute;n contra Ataques DDOS</li>
      <li>Seguridad por Reglas.</li>
      <li>Backup Mensual.</li>
      `,
        Head: false,
      },
    ],
  },
  // {
  //   fila: [
  //     { text: "Dominios", Head: true },
  //     { text: "1 dominio", Head: false },
  //     { text: "1 dominio", Head: false },
  //     { text: "1 dominio", Head: false },
  //   ],
  // },
  // {
  //   fila: [
  //     { text: "Subdominios", Head: true },
  //     { text: "1 sub", Head: false },
  //     { text: "1 sub", Head: false },
  //     { text: "1 sub", Head: false },
  //   ],
  // },
  // {
  //   fila: [
  //     { text: "Servidor Web", Head: true },
  //     {
  //       text: "2-3 veces mas r&aacute;pido que un hosting normal (LITESPEED)",
  //       colspan: true,
  //     },
  //   ],
  // },
  // {
  //   fila: [
  //     { text: "Aseguramiento de envio de correos", Head: true },
  //     {
  //       text: "Sistema  Mail relay con un 99% de &eacute;xito de env&iacute;o de correo",
  //       Head: false,
  //       colspan: true,
  //     },
  //   ],
  // },
  // {
  //   fila: [
  //     { text: "Control de SPAM", Head: true },
  //     {
  //       text:
  //         "Optimizado para evitar la llegada y envio de correos no deseados",
  //       Head: false,
  //       colspan: true,
  //     },
  //   ],
  // },
  // {
  //   fila: [
  //     { text: "Firewall", Head: true },
  //     { text: "Activado. Evita ataques comunes", Head: false, colspan: true },
  //   ],
  // },
  // {
  //   fila: [
  //     { text: "SSL", Head: true },
  //     { text: "Gratuito", Head: false, colspan: true },
  //   ],
  // },
  {
    fila: [
      { text: "Inversi&oacute;n anual", Head: true },
      { text: DataPlanInicio[0].precio, Head: false },
      { text: DataPlanInicio[1].precio, Head: false },
      { text: DataPlanInicio[2].precio, Head: false },
    ],
  },
  {
    fila: [
      { text: "Addons", Head: true },
      { text: "SSL Adicional $/10.00", Head: false },
      { text: "SSL Adicional $10.00", Head: false },
      { text: "SSL Adicional $10.00", Head: false },
    ],
  },
  {
    fila: [
      { text: "", Head: false },
      {
        text: "Comprar",
        Head: false,
        Btn: true,
        url: Mensaje(DataPaquetes[0].title, DataPlanInicio[0].title),
      },
      {
        text: "Comprar",
        Head: false,
        Btn: true,
        url: Mensaje(DataPaquetes[0].title, DataPlanInicio[1].title),
      },
      {
        text: "Comprar",
        Head: false,
        Btn: true,
        url: Mensaje(DataPaquetes[0].title, DataPlanInicio[2].title),
      },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[2].title) },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[].title },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[].title },
    ],
  },
]
export const DataPrecioGestor = [
  {
    fila: [
      { text: "PAQUETE", Head: true },
      {
        text: DataPlanGestor[0].title,
        precio: DataPlanGestor[0].precio,
        Head: true,
      },
      {
        text: DataPlanGestor[1].title,
        precio: DataPlanGestor[1].precio,
        Head: true,
      },
      {
        text: DataPlanGestor[2].title,
        precio: DataPlanGestor[2].precio,
        Head: true,
      },
      // { text: DataPlanInicio[3].title, Head: true },
      // { text: DataPlanInicio[4].title, Head: true },
      // { text: DataPlanInicio[5].title, Head: true },
    ],
  },
  {
    fila: [
      { text: "", Head: true },
      {
        text: DataPlanGestor[0].text,
        Head: false,
      },
      {
        text: DataPlanGestor[1].text,
        Head: false,
      },
      {
        text: DataPlanGestor[2].text,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "Servidor", Head: true },
      {
        text: `
      <li>  2.5GB de Espacio NVMe</li>
      <li>  25GB Transferencia</li>
      <li>  1 parking  dominio</li>
      <li>  1 parking  subdominio</li>
      <li>  1 BD</li>
      <li>  Softaculous: Instale Wordpress, Moodle, Joomla, Drupal y otras 300 aplicaciones con un solo click</li>
      <li>  Lenguaje PHP</li>
      `,
        Head: false,
      },
      {
        text: `
        <li>5GB de Espacio NVMe</li>
        <li>50GB</li>
        <li>3 parking  dominio</li>
        <li>3 parking  subdominio</li>
        <li>3BD</li>
        <li>Softaculous: Instale Wordpress, Moodle, Joomla, Drupal y otras 300 aplicaciones con un solo click.</li>
        <li>Lenguaje PHP</li>
        
      `,
        Head: false,
      },
      {
        text: `
        <li>10GB de Espacio NVMe</li>
        <li>100GB</li>
        <li>4 parking  dominio</li>
        <li>4 parking  subdominio</li>
        <li>4 BD</li>
        <li>Softaculous: Instale Wordpress, Moodle, Joomla, Drupal y otras 300 aplicaciones con un solo click</li>
        <li>Lenguaje PHP</li>
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "", Head: false, More:true},
     
    ],
  },
  {
    fila: [
      { text: "OPTIMIZACI&oacute;N", Head: true },
      {
        text: `
      <li>100% + Velocidad Servidor Web (LiteSpeed).</li>
      <li>LS Cache: Mejora m&aacute;s la velocidad de tu web en Wordpress al 100% gratis.</li>
      <li>Discos NVME (3600% m&aacute;s veloz vs Hosting Normales)</li>
      
      `,
        Head: false,
      },
      {
        text: `
      <li>100% + Velocidad Servidor Web (LiteSpeed).</li>
      <li>LS Cache: Mejora m&aacute;s la velocidad de tu web en Wordpress al 100% gratis.</li>
      <li>50% + Velocidad de Procesador que los planes b&aacute;sicos.</li>
      <li>Discos NVME (3600% m&aacute;s veloz vs Hosting Normales)</li>
      
      `,
        Head: false,
      },
      {
        text: `
      <li>100%+ Velocidad Servidor Web (LiteSpeed).</li>
      <li>LS Cache: Mejora m&aacute;s la velocidad de tu web en Wordpress al 100% gratis.</li>
      <li>100% + Velocidad de Procesador que  planes b&aacute;sico.</li>
      <li>Discos NVME (3600% m&aacute;s veloz vs Hosting Normales)</li>
      <li>2MB I/O </li>
      
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "CORREOS", Head: true },
      {
        text: `
      <li>20 correos</li>
      <li>SSL Gratuito para un dominio</li>
      <li>MapachioBox Web</li>
      <li>MapachioBox APP</li>
      <li>RoundCube</li>
      <li>Envi&oacute; y Recepci&oacute;n Proactiva de Mail: Servicio Mejorado de Envi&oacute;</li>
      `,
        Head: false,
      },
      {
        text: `
      <li>40 correos</li>
      <li>SSL Gratuito para un dominio</li>
      <li>MapachioBox Web</li>
      <li>MapachioBox APP</li>
      <li>RoundCube</li>
      <li>Envi&oacute; y Recepci&oacute;n Proactiva de Mail: Servicio Mejorado de Envi&oacute;</li>
      
      `,
        Head: false,
      },
      {
        text: `
      <li>100 correos</li>
      <li>MapachioBox Web</li>
      <li>MapachioBox APP</li>
      <li>RoundCube</li>
      <li>Envi&oacute; y Recepci&oacute;n Proactiva de Mail: Servicio Mejorado de Envi&oacute;</li>
      
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "SEGURIDAD", Head: true },
      {
        text: `
      <li>Antivirus</li>
      <li>Firewall</li>
      <li>Protecci&oacute;n contra Ataques DDOS</li>
      <li>Seguridad por Reglas.</li>
      <li>Backup Mensual.</li>
      
      `,
        Head: false,
      },
      {
        text: `
      <li>Antivirus</li>
      <li>Firewall</li>
      <li>Protecci&oacute;n contra Ataques DDOS</li>
      <li>Seguridad por Reglas.</li>
      <li>Backup Mensual.</li>
      
      `,
        Head: false,
      },
      {
        text: `
      <li>SSL Gratuito para un dominio</li>
      <li>Antivirus</li>
      <li>Firewall</li>
      <li>Protecci&oacute;n contra Ataques DDOS</li>
      <li>Seguridad por Reglas.</li>
      <li>Backup Semanal.</li>
      
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "Soporte", Head: true },
      { text: `Soporte Profesional por Whatsapp.`, Head: false },
      { text: `Soporte Profesional por Whatsapp.`, Head: false },
      { text: `Soporte Profesional por Whatsapp.`, Head: false },
    ],
  },
  {
    fila: [
      { text: "Inversi&oacute;n anual", Head: true },
      { text: DataPlanGestor[0].precio, Head: false },
      { text: DataPlanGestor[1].precio, Head: false },
      { text: DataPlanGestor[2].precio, Head: false },
    ],
  },
  {
    fila: [
      { text: "Addons", Head: true },
      { text: "20 Correos Adicionales $10.00", Head: false },
      {
        text: `
      <li>SSL Adicional $10.00.00</li>
      <li>20 Correos Adicionales $10.00</li>
      `,
        Head: false,
      },
      {
        text: `
      <li>20 Correos Adicionales $10.00/li>
      <li>1 Dominio $25.00/li>
      <li>1 Subdominio $10.00/li>
      <li>SSL Adicional $10.00</li>
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "", Head: false },
      {
        text: "Comprar",
        Head: false,
        Btn: true,
        url: Mensaje(DataPaquetes[1].title, DataPlanGestor[0].title),
      },
      {
        text: "Comprar",
        Head: false,
        Btn: true,
        url: Mensaje(DataPaquetes[1].title, DataPlanGestor[1].title),
      },
      {
        text: "Comprar",
        Head: false,
        Btn: true,
        url: Mensaje(DataPaquetes[1].title, DataPlanGestor[2].title),
      },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[2].title) },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[].title },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[].title },
    ],
  },
]
export const DataPrecioEmpresa = [
  {
    fila: [
      { text: "PAQUETE", Head: true },
      {
        text: DataPlanEmpresa[0].title,
        precio: DataPlanEmpresa[0].precio,
        Head: true,
      },
      {
        text: DataPlanEmpresa[1].title,
        precio: DataPlanEmpresa[1].precio,
        Head: true,
      },
      {
        text: DataPlanEmpresa[2].title,
        precio: DataPlanEmpresa[2].precio,
        Head: true,
      },
      // { text: DataPlanInicio[3].title, Head: true },
      // { text: DataPlanInicio[4].title, Head: true },
      // { text: DataPlanInicio[5].title, Head: true },
    ],
  },
  {
    fila: [
      { text: "", Head: true },
      {
        text: DataPlanEmpresa[0].text,
        Head: false,
      },
      {
        text: DataPlanEmpresa[1].text,
        Head: false,
      },
      {
        text: DataPlanEmpresa[2].text,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "Servidor", Head: true },
      {
        text: `
        <li>25GB de Espacio NVMe</li>
        <li>Transferencia Ilimitada</li>
        <li>5 parking  dominios</li>
        <li>5 parking  subdominio</li>
        <li>5 BD</li>
        <li>Softaculous: Instale Wordpress, Moodle, Joomla, Drupal y otras 300 aplicaciones con un solo click</li>
        <li>Lenguaje PHP</li>
        
        `,
        Head: false,
      },
      {
        text: `
        <li>50 GB de Espacio NVMe</li>
        <li>Transferencia Ilimitada</li>
        <li>10  parking dominios</li>
        <li>10 parking  subdominio</li>
        <li>10 BD</li>
        <li>Softaculous: Instale Wordpress, Moodle, Joomla, Drupal y otras 300 aplicaciones con un solo click</li>
        <li>Lenguaje PHP</li>
      `,
        Head: false,
      },
      {
        text: `
        <li>100 GB de Espacio NVMe</li>
        <li>Transferencia Ilimitada</li>
        <li>20 parking  dominios</li>
        <li>20 parking  subdominio</li>
        <li>20 BD</li>
        <li>Softaculous: Instale Wordpress, Moodle, Joomla, Drupal y otras 300 aplicaciones con un solo click</li>
        <li>Lenguaje PHP</li>
        
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "", Head: false, More:true},
     
    ],
  },
  {
    fila: [
      { text: "OPTIMIZACI&oacute;N", Head: true },
      {
        text: `
        <li>100%  + Velocidad Servidor Web (LiteSpeed).</li>
        <li>LS Cache: Mejora m&aacute;s la velocidad de tu web en Wordpress al 100% gratis.</li>
        <li>100% + Velocidad de Procesador que  planes b&aacute;sico.</li>
        <li>Discos NVME (3600% m&aacute;s veloz vs Hosting Normales)</li>
        <li>2MB I/O</li>
        
      `,
        Head: false,
      },
      {
        text: `
        <li>100%  + Velocidad Servidor Web (LiteSpeed).</li>
        <li>LS Cache: Mejora m&aacute;s la velocidad de tu web en Wordpress al 100% gratis.</li>
        <li>100% + Velocidad de  Procesador que  planes b&aacute;sico.</li>
        <li>Discos NVME (3600% m&aacute;s veloz vs Hosting Normales)</li>
        <li>2MB I/O</li>
        
      `,
        Head: false,
      },
      {
        text: `
        <li>100%  + Velocidad Servidor Web (LiteSpeed).</li>
        <li>LS Cache: Mejora m&aacute;s la velocidad de tu web en Wordpress al 100% gratis.</li>
        <li>150% + Velocidad de Procesador que  planes b&aacute;sico</li>
        <li>2GB Ram Compartido.</li>
        <li>Discos NVME (3600% m&aacute;s veloz vs Hosting Normales)</li>
        <li>2MB I/O</li>
        
        `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "CORREOS", Head: true },
      {
        text: `
        <li>200 correos</li>
        <li>MapachioBox Web</li>
        <li>MapachioBox APP</li>
        <li>RoundCube</li>
        <li>Envi&oacute; y Recepci&oacute;n Proactiva de Mail: Servicio Mejorado de Envi&oacute;</li>
        `,
        Head: false,
      },
      {
        text: `
        <li>400 correos</li>
        <li>MapachioBox Web</li>
        <li>MapachioBox APP</li>
        <li>RoundCube</li>
        <li>Envi&oacute; y Recepci&oacute;n Proactiva de Mail: Servicio Mejorado de Envi&oacute;</li>
      `,
        Head: false,
      },
      {
        text: `
        <li>Correos Ilimitados</li>
        <li>MapachioBox Web</li>
        <li>MapachioBox APP</li>
        <li>RoundCube</li>
        <li>Envi&oacute; y Recepci&oacute;n Proactiva de Mail: Servicio Mejorado de Envi&oacute;</li>
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "SEGURIDAD", Head: true },
      {
        text: `
        <li>SSL Gratuito para un dominio</li>
        <li>Antivirus</li>
        <li>Firewall</li>
        <li>Protecci&oacute;n contra Ataques DDOS</li>
        <li>Seguridad por Reglas.</li>
        <li>Backup Semanal.</li>
        
      `,
        Head: false,
      },
      {
        text: `
        <li>SSL Gratuito para un dominio</li>
        <li>Antivirus</li>
        <li>Firewall</li>
        <li>Protecci&oacute;n contra Ataques DDOS</li>
        <li>Seguridad por Reglas.</li>
        <li>Backup Semanal.</li>
      `,
        Head: false,
      },
      {
        text: `
        <li>SSL Gratuito para un dominio</li>
        <li>Antivirus</li>
        <li>Firewall</li>
        <li>Proteccion contra Ataques DDOS</li>
        <li>Seguridad por Reglas.</li>
        <li>Backup Semanal. </li>
        
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "Soporte", Head: true },
      { text: `Soporte Profesional por Whatsapp.`, Head: false },
      { text: `Soporte Profesional por Whatsapp.`, Head: false },
      { text: `Soporte Profesional por Whatsapp.`, Head: false },
    ],
  },
  {
    fila: [
      { text: "Inversi&oacute;n anual", Head: true },
      { text: DataPlanEmpresa[0].precio, Head: false },
      { text: DataPlanEmpresa[1].precio, Head: false },
      { text: DataPlanEmpresa[2].precio, Head: false },
    ],
  },
  {
    fila: [
      { text: "Inversi&oacute;n mensual", Head: true },
      { text: "$17.19", Head: false },
      { text: "$26.04", Head: false },
      { text: "$50.00", Head: false },
    ],
  },
  {
    fila: [
      { text: "Addons", Head: true },
      {
        text: `
      <li>25 Correos Adicionales $10.00</li>
      <li>1 Dominio $25.00</li>
      <li>1 Subdominio $10.00</li>
      <li>SSL Adicional $10.00</li>
      `,
        Head: false,
      },
      {
        text: `
      <li>25 Correos Adicionales $10.00</li>
      <li>1 Dominio $25.00</li>
      <li>1 Subdominio $10.00</li>
      <li>SSL Adicional $10.00</li>
      `,
        Head: false,
      },
      {
        text: `
      <li>1 BD Adicional $10.00</li>
      <li>1 Dominio $25.00</li>
      <li>1 Subdominio $10.00</li>
      <li>SSL Adicional $10.00</li>
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "", Head: false },
      {
        text: "Comprar",
        Head: false,
        Btn: true,
        url: Mensaje(DataPaquetes[2].title, DataPlanEmpresa[0].title),
      },
      {
        text: "Comprar",
        Head: false,
        Btn: true,
        url: Mensaje(DataPaquetes[2].title, DataPlanEmpresa[1].title),
      },
      {
        text: "Comprar",
        Head: false,
        Btn: true,
        url: Mensaje(DataPaquetes[2].title, DataPlanEmpresa[2].title),
      },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[2].title) },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[].title },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[].title },
    ],
  },
]
export const DataPrecioCorporativo = [
  {
    fila: [
      { text: "PAQUETE", Head: true },
      {
        text: DataPlanCorporativo[0].title,
        precio: DataPlanCorporativo[0].precio,
        Head: true,
      },
      {
        text: DataPlanCorporativo[1].title,
        precio: DataPlanCorporativo[1].precio,
        Head: true,
      },
      // correcion vpn
      // { text: DataPlanCorporativo[2].title, Head: true },
      // { text: DataPlanInicio[3].title, Head: true },
      // { text: DataPlanInicio[4].title, Head: true },
      // { text: DataPlanInicio[5].title, Head: true },
    ],
  },
  {
    fila: [
      { text: "", Head: true, },
      {
        text: DataPlanCorporativo[0].text,
        Head: false,
      },
      {
        text: DataPlanCorporativo[1].text,
        Head: false,
      },
      // correccion
      // {
      //   text:DataPlanCorporativo[2].text,
      //   Head: false,
      // },
    ],
  },
  {
    fila: [
      { text: "Servidor", Head: true },
      {
        text: `
        <li>200 GB de Espacio NVMe</li>
        <li>Transferencia Ilimitada</li>
        <li>40 parking  dominios</li>
        <li>40 parking  subdominio</li>
        <li>40 BD</li>
        <li>Softaculous: Instale Wordpress, Moodle, Joomla, Drupal y otras 300 aplicaciones con un solo click</li>
        <li>Lenguaje PHP</li>
        
        `,
        Head: false,
      },
      {
        text: `
        <li>400 GB de Espacio NVMe</li>
        <li>Transferencia Ilimitada</li>
        <li>80 parking  dominios</li>
        <li>80 parking  subdominio</li>
        <li>80 BD</li>
        <li>Softaculous: Instale Wordpress, Moodle, Joomla, Drupal y otras 300 aplicaciones con un solo click</li>
        <li>Lenguaje PHP</li>
        
        `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "", Head: false, More:true},
     
    ],
  },
  {
    fila: [
      { text: "OPTIMIZACI&oacute;N", Head: true },
      {
        text: `
        <li>100%  + Velocidad Servidor Web (LiteSpeed).</li>
        <li>LS Cache: Mejora m&aacute;s la velocidad de tu web en Wordpress al 100% gratis.</li>
        <li>150% + Velocidad de Procesador que  planes b&aacute;sico</li>
        <li>2GB Ram Compartido.</li>
        <li>Discos NVME (3600% m&aacute;s veloz vs Hosting Normales)</li>
        <li>3MB I/O</li>
        
      `,
        Head: false,
      },
      {
        text: `
        <li>100% + Velocidad Servidor Web (LiteSpeed).</li>
        <li>LS Cache: Mejora m&aacute;s la velocidad de tu web en Wordpress al 100% gratis.</li>
        <li>150% + Velocidad de Procesador que  planes b&aacute;sico</li>
        <li>2GB Ram Compartido.</li>
        <li>Discos NVME (3600% m&aacute;s veloz vs Hosting Normales)</li>
        <li>4MB I/O</li>
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "CORREOS", Head: true },
      {
        text: `
        <li>Correo Ilimitados</li>
        <li>MapachioBox Web</li>
        <li>MapachioBox APP</li>
        <li>RoundCube</li>
        <li>Envi&oacute; y Recepci&oacute;n Proactiva de Mail: Servicio Mejorado de Envi&oacute;</li>
        `,
        Head: false,
      },
      {
        text: `
        <li>Correos Ilimitados</li>
        <li>MapachioBox Web</li>
        <li>MapachioBox APP</li>
        <li>RoundCube</li>
        <li>Envi&oacute; y Recepci&oacute;n Proactiva de Mail: Servicio Mejorado de Envi&oacute;</li>
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "SEGURIDAD", Head: true },
      {
        text: `
        <li>SEGURIDAD</li>
        <li>SSL Gratutito para 5 dominios</li>
        <li>Antivirus</li>
        <li>Firewall</li>
        <li>Protecci&oacute;n contra Ataques DDOS</li>
        <li>Seguridad por Reglas.</li>
        <li>Backup Diario.</li>
        
      `,
        Head: false,
      },
      {
        text: `
        <li>SSL Gratutito para todos los dominios</li>
        <li>Antivirus</li>
        <li>Firewall</li>
        <li>Protecci&oacute;n contra Ataques DDOS</li>
        <li>Seguridad por Reglas.</li>
        <li>Backup Diario.</li>
        
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "Soporte", Head: true },
      { text: `Soporte Profesional por Whatsapp.`, Head: false },
      { text: `Soporte Profesional por Whatsapp.`, Head: false },
    ],
  },
  {
    fila: [
      { text: "Inversi&oacute;n anual", Head: true },
      { text: DataPlanCorporativo[0].precio, Head: false },
      { text: DataPlanCorporativo[1].precio, Head: false },
    ],
  },
  {
    fila: [
      { text: "Inversi&oacute;n mensual", Head: true },
      { text: "$93.75", Head: false },
      { text: "$166.67", Head: false },
    ],
  },
  {
    fila: [
      { text: "Addons", Head: true },
      {
        text: `
      <li>1 BD Adicional $10</li>
      <li>1 Dominio $25</li>
      <li>1 Subdominio $10</li>
      `,
        Head: false,
      },
      {
        text: `
      <li>1 BD Adicional $10</li>
      <li>1 Dominio $25</li>
      <li>1 Subdominio $10</li>
      `,
        Head: false,
      },
      // {
      //   text: `
      // <li>1 BD Adicional $10</li>
      // <li>1 Dominio $25</li>
      // <li>1 Subdominio $10    </li>
      // `,
      //   Head: false,
      // },
    ],
  },
  {
    fila: [
      { text: "", Head: false },
      {
        text: "Comprar",
        Head: false,
        Btn: true,
        url: Mensaje(DataPaquetes[3].title, DataPlanCorporativo[0].title),
      },
      {
        text: "Comprar",
        Head: false,
        Btn: true,
        url: Mensaje(DataPaquetes[3].title, DataPlanCorporativo[1].title),
      },
      // {
      //   text: "Comprar",
      //   Head: false,
      //   Btn: true,
      //   url: Mensaje(DataPaquetes[3].title, DataPlanCorporativo[2].title),
      // },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[2].title) },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[].title },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[].title },
    ],
  },
]
export const DataPrecioVPS = [
  {
    fila: [
      { text: "PAQUETE", Head: true },
      { text: DataPlanVPS[0].title, precio: DataPlanVPS[0].precio, Head: true },
      { text: DataPlanVPS[1].title, precio: DataPlanVPS[1].precio, Head: true },
      { text: DataPlanVPS[2].title, precio: DataPlanVPS[2].precio, Head: true },
      // { text: DataPlanInicio[3].title, Head: true },
      // { text: DataPlanInicio[4].title, Head: true },
      // { text: DataPlanInicio[5].title, Head: true },
    ],
  },
  {
    fila: [
      { text: "", Head: true },
      {
        text: DataPlanVPS[0].text,
        Head: false,
      },
      {
        text: DataPlanVPS[1].text,
        Head: false,
      },
      {
        text: DataPlanVPS[2].text,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "Servidor", Head: true },
      {
        text: `
        <li>800 GB de Espacio NVMe</li>
        <li>Transferencia Ilimitada</li>
        <li>160 parking dominios</li>
        <li>160 parking  subdominio</li>
        <li>160 BD</li>
        <li>Softaculous: Instale Wordpress, Moodle, Joomla, Drupal y otras 300 aplicaciones con un solo click</li>
        <li>Lenguaje PHP</li>
        `,
        Head: false,
      },
      {
        text: `
        <li>1.6 TB de Espacio NVMe</li>
        <li>Transferencia Ilimitada</li>
        <li>Dominios Ilimitados</li>
        <li>Subdominios Ilimitados</li>
        <li>IP Propia</li>
        <li>BD Ilimitadas</li>
        <li>Softaculous: Instale Wordpress, Moodle, Joomla, Drupal y otras 300 aplicaciones con un solo click</li>
        <li>Lenguaje PHP</li>
        `,
        Head: false,
      },
      {
        text: `
        <li>2.4 TB de Espacio NVMe</li>
        <li>Transferencia Ilimitada</li>
        <li>Dominios Ilimitados</li>
        <li>Subdominios Ilimitados</li>
        <li>IP Propia</li>
        <li>BD Ilimitadas</li>
        <li>Softaculous: Instale Wordpress, Moodle, Joomla, Drupal y otras 300 aplicaciones con un solo click</li>
        <li>Lenguaje PHP</li>
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "", Head: false, More:true},
     
    ],
  },
  {
    fila: [
      { text: "OPTIMIZACI&oacute;N", Head: true },
      {
        text: `
        <li>100% + Velocidad Servidor Web (LiteSpeed).</li>
        <li>LS Cache: Mejora m&aacute;s la velocidad de tu web en Wordpress al 100% gratis.</li>
        <li>200% + Velocidad de Procesador que  planes b&aacute;sico</li>
        <li>4GB Ram Compartido.</li>
        <li>Discos NVME (3600% m&aacute;s veloz vs Hosting Normales)</li>
        <li>6MB I/O</li>
        
        `,
        Head: false,
      },
      {
        text: `
        <li>100%  + Velocidad Servidor Web (LiteSpeed).</li>
        <li>LS Cache: Mejora m&aacute;s la velocidad de tu web en Wordpress al 100% gratis.</li>
        <li>200% + Velocidad de Procesador que  planes b&aacute;sico</li>
        <li>6GB Ram Compartido.</li>
        <li>Discos NVME (3600% m&aacute;s veloz vs Hosting Normales)</li>
        <li>10MB I/O</li>
        
      `,
        Head: false,
      },
      {
        text: `
        <li>100%  + Velocidad Servidor Web (LiteSpeed).</li>
        <li>LS Cache: Mejora m&aacute;s la velocidad de tu web en Wordpress al 100% gratis.</li>
        <li>200% + Velocidad de Procesador que  planes b&aacute;sico</li>
        <li>8GB Ram Compartido.</li>
        <li>Discos NVME (3600% m&aacute;s veloz vs Hosting Normales)</li>
        <li>15MB I/O</li>
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "CORREOS", Head: true },
      {
        text: `
        <li>Correos Ilimitados</li>
        <li>MapachioBox Web</li>
        <li>MapachioBox APP</li>
        <li>RoundCube</li>
        <li>Envi&oacute; y Recepción Proactiva de Mail: Servicio Mejorado de Envió</li>
      `,
        Head: false,
      },
      {
        text: `
        <li>Correos Ilimitados</li>
        <li>MapachioBox Web</li>
        <li>MapachioBox APP</li>
        <li>RoundCube</li>
        <li>Envió y Recepción Proactiva de Mail: Servicio Mejorado de Envió</li>
        `,
        Head: false,
      },
      {
        text: `
        <li>Correos Ilimitados</li>
        <li>MapachioBox Web</li>
        <li>MapachioBox APP</li>
        <li>RoundCube</li>
        <li>Envió y Recepción Proactiva de Mail: Servicio Mejorado de Envió</li>
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "SEGURIDAD", Head: true },
      {
        text: `
        <li>SSL Gratutito para todos los dominios</li>
        <li>Antivirus</li>
        <li>Firewall</li>
        <li>Protección contra Ataques DDOS</li>
        <li>Seguridad por Reglas.</li>
        <li>Backup Diario.</li>
      `,
        Head: false,
      },
      {
        text: `
        <li>SSL Gratutito para todos los dominios</li>
        <li>Antivirus</li>
        <li>Firewall</li>
        <li>Protección contra Ataques DDOS</li>
        <li>Seguridad por Reglas.</li>
        <li>Backup Diario.</li>
        
      `,
        Head: false,
      },
      {
        text: `
        <li>SSL Gratutito para todos los dominios</li>
        <li>Antivirus</li>
        <li>Firewall</li>
        <li>Protección contra Ataques DDOS</li>
        <li>Seguridad por Reglas.</li>
        <li>Backup Diario.</li>
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "Soporte", Head: true },
      { text: `Soporte Profesional por Whatsapp.`, Head: false },
      { text: `Soporte Profesional por Whatsapp.`, Head: false },
      { text: `Soporte Profesional por Whatsapp.`, Head: false },
    ],
  },
  {
    fila: [
      { text: "Inversión anual", Head: true },
      { text: DataPlanVPS[0].precio, Head: false },
      { text: DataPlanVPS[1].precio, Head: false },
      { text: DataPlanVPS[2].precio, Head: false },
    ],
  },
  {
    fila: [
      { text: "Inversión mensual", Head: true },
      { text: "$250.00", Head: false },
      { text: "$458.33", Head: false },
      { text: "$666.67", Head: false },
    ],
  },
  {
    fila: [
      { text: "", Head: false },
      {
        text: "Comprar",
        Head: false,
        Btn: true,
        url: Mensaje(DataPaquetes[4].title, DataPlanVPS[0].title),
      },
      {
        text: "Comprar",
        Head: false,
        Btn: true,
        url: Mensaje(DataPaquetes[4].title, DataPlanVPS[1].title),
      },
      {
        text: "Comprar",
        Head: false,
        Btn: true,
        url: Mensaje(DataPaquetes[4].title, DataPlanVPS[2].title),
      },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[2].title) },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[].title },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[].title },
    ],
  },
]
export const DataPrecioEducacion = [
  {
    fila: [
      { text: "PAQUETE", Head: true },
      {
        text: DataPlanEducacion[0].title,
        precio: DataPlanEducacion[0].precio,
        Head: true,
      },
      {
        text: DataPlanEducacion[1].title,
        precio: DataPlanEducacion[1].precio,
        Head: true,
      },
      {
        text: DataPlanEducacion[2].title,
        precio: DataPlanEducacion[2].precio,
        Head: true,
      },
    ],
  },
  {
    fila: [
      { text: "", Head: true },
      {
        text: DataPlanEducacion[0].text,
        Head: false,
      },
      {
        text: DataPlanEducacion[1].text,
        Head: false,
      },
      {
        text: DataPlanEducacion[2].text,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "Servidor", Head: true },
      {
        text: `
        <li>5 GB de Espacio NVMe</li>
        <li>Transferencia Ilimitada</li>
        <li>2 dominios</li>
        <li>5 subdominio</li>
        <li>5 BD</li>
        <li>Softaculous: Instale Wordpress, Moodle, Joomla, Drupal y otras 300 aplicaciones con un solo click</li>
        <li>Lenguaje PHP</li>
        `,
        Head: false,
      },
      {
        text: `
        <li>25 GB de Espacio NVMe</li>
        <li>Transferencia Ilimitada</li>
        <li>3 dominios</li>
        <li>10 subdominio</li>
        <li>10 BD</li>
        <li>Softaculous: Instale Wordpress, Moodle, Joomla, Drupal y otras 300 aplicaciones con un solo click</li>
        <li>Lenguaje PHP</li>
        
      `,
        Head: false,
      },
      {
        text: `
        <li>50 GB de Espacio NVMe</li>
        <li>Transferencia Ilimitada</li>
        <li>4 dominios</li>
        <li>20 subdominio</li>
        <li>20 BD</li>
        <li>Softaculous: Instale Wordpress, Moodle, Joomla, Drupal y otras 300 aplicaciones con un solo click</li>
        <li>Lenguaje PHP</li>
        `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "", Head: false, More:true},
     
    ],
  },
  {
    fila: [
      { text: "OPTIMIZACIÓN", Head: true },
      {
        text: `
        <li>100%  + Velocidad Servidor Web (LiteSpeed).</li>
        <li>LS Cache: Mejora m&aacute;s la velocidad de tu web en Wordpress al 100% gratis.</li>
        <li>150% + Velocidad de Procesador que  planes b&aacute;sico.</li>
        <li>5RAM Compartido</li>
        <li>Discos NVME (3600% m&aacute;s veloz vs Hosting Normales)</li>
        <li>5MB I/O Extendido</li>
        `,
        Head: false,
      },
      {
        text: `
        <li>100% + Velocidad Servidor Web (LiteSpeed).</li>
        <li>LS Cache: Mejora m&aacute;s la velocidad de tu web en Wordpress al 100% gratis.</li>
        <li>200% + Velocidad de Procesador que  planes b&aacute;sico.</li>
        <li>10MB I/O Extendido</li>
        <li>Discos NVME (3600% m&aacute;s veloz vs Hosting Normales)</li>
        <li>10RAM</li>
        
      `,
        Head: false,
      },
      {
        text: `
        <li>100%  + Velocidad Servidor Web (LiteSpeed).</li>
        <li>LS Cache: Mejora m&aacute;s la velocidad de tu web en Wordpress al 100% gratis.</li>
        <li>300% + Velocidad de Procesador que  planes b&aacute;sico.</li>
        <li>Discos NVME (3600% m&aacute;s veloz vs Hosting Normales)</li>
        <li>10MB I/O Extendido</li>
        <li>15RAM</li>
        `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "CORREOS", Head: true },
      {
        text: `
        <li>200 correos</li>
        <li>MapachioBox Web</li>
        <li>MapachioBox APP</li>
        <li>RoundCube</li>
        <li>Envió y Recepción Proactiva de Mail: Servicio Mejorado de Envió</li>
        `,
        Head: false,
      },
      {
        text: `
        <li>400 correos</li>
        <li>MapachioBox Web</li>
        <li>MapachioBox APP</li>
        <li>RoundCube</li>
        <li>Envió y Recepción Proactiva de Mail: Servicio Mejorado de Envió</li>
        
      `,
        Head: false,
      },
      {
        text: `
        <li>800 correos</li>
        <li>SSL Gratutito para todos los dom&iacute;nios</li>
        <li>MapachioBox Web</li>
        <li>MapachioBox APP</li>
        <li>RoundCube</li>
        <li>Envi&oacute; y Recepci&oacute;n Proactiva de Mail: Servicio Mejorado de Envi&oacute;</li>
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "SEGURIDAD", Head: true },
      {
        text: `
        <li>SSL Gratutito para todos los dominios</li>
        <li>Antivirus</li>
        <li>Firewall</li>
        <li>Protecci&oacute;n contra Ataques DDOS</li>
        <li>Seguridad por Reglas.</li>
        <li>Backup Diario.</li>
      `,
        Head: false,
      },
      {
        text: `
        <li>SSL Gratutito para todos los dominios</li>
        <li>Antivirus</li>
        <li>Firewall</li>
        <li>Protecci&oacute;n contra Ataques DDOS</li>
        <li>Seguridad por Reglas.</li>
        <li>Backup Diario.</li>
      `,
        Head: false,
      },
      {
        text: `
        <li>SSL Gratutito para todos los dominios</li>
        <li>Antivirus</li>
        <li>Firewall</li>
        <li>Protecci&oacute;n contra Ataques DDOS</li>
        <li>Seguridad por Reglas.</li>
        <li>Backup Diario.</li>
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "Soporte", Head: true },
      { text: `Soporte Profesional por Whatsapp.`, Head: false },
      { text: `Soporte Profesional por Whatsapp.`, Head: false },
      { text: `Soporte Profesional por Whatsapp.`, Head: false },
    ],
  },
  {
    fila: [
      { text: "Inversi&oacute;n anual", Head: true },
      { text: DataPlanEducacion[0].precio, Head: false },
      { text: DataPlanEducacion[1].precio, Head: false },
      { text: DataPlanEducacion[2].precio, Head: false },
    ],
  },
  {
    fila: [
      { text: "Inversi&oacute;n mensual", Head: true },
      { text: "$15.63", Head: false },
      { text: "$26.04", Head: false },
      { text: "$36.46", Head: false },
    ],
  },
  {
    fila: [
      { text: "Addons", Head: true },

      {
        text: `
      <li>25 Correos Adicionales $10.00</li>
      <li>1 Dominio Adicional $50.00</li>
      <li>1 Subdominio $25.00</li>
      `,
        Head: false,
      },
      {
        text: `
      <li>25 Correos Adicionales $10.00</li>
      <li>1 Dominio Adicional $50.00</li>
      <li>1 Subdominio $25.00</li>
      `,
        Head: false,
      },
      {
        text: `
      <li>25 Correos Adicionales $10.00</li>
      <li>1 Dominio Adicional $50.00</li>
      <li>1 Subdominio $25.00</li>
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "", Head: false },
      {
        text: "Comprar",
        Head: false,
        Btn: true,
        url: Mensaje(DataPaquetes[5].title, DataPlanEducacion[0].title),
      },
      {
        text: "Comprar",
        Head: false,
        Btn: true,
        url: Mensaje(DataPaquetes[5].title, DataPlanEducacion[1].title),
      },
      {
        text: "Comprar",
        Head: false,
        Btn: true,
        url: Mensaje(DataPaquetes[5].title, DataPlanEducacion[2].title),
      },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[2].title) },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[].title },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[].title },
    ],
  },
]
export const DataPrecioDesarrollo = [
  {
    fila: [
      { text: "PAQUETE", Head: true },
      {
        text: DataPlanDesarrollo[0].title,
        precio: DataPlanDesarrollo[0].precio,
        Head: true,
      },
      {
        text: DataPlanDesarrollo[1].title,
        precio: DataPlanDesarrollo[1].precio,
        Head: true,
      },
      {
        text: DataPlanDesarrollo[2].title,
        precio: DataPlanDesarrollo[2].precio,
        Head: true,
      },
    ],
  },
  {
    fila: [
      { text: "", Head: true },
      {
        text: DataPlanDesarrollo[0].text,
        Head: false,
      },
      {
        text: DataPlanDesarrollo[1].text,
        Head: false,
      },
      {
        text: DataPlanDesarrollo[2].text,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "Servidor", Head: true },
      {
        text: `
        <li>5 GB de Espacio NVMe</li>
        <li>Transferencia Ilimitada</li>
        <li>1 dominios</li>
        <li>5 subdominio</li>
        <li>5 BD</li>
        <li>Softaculous: Instale Wordpress, Moodle, Joomla, Drupal y otras 300 aplicaciones con un solo click</li>
        
        `,
        Head: false,
      },
      {
        text: `
        <li>25 GB de Espacio NVMe</li>
        <li>Transferencia Ilimitada</li>
        <li>3 dominios</li>
        <li>10 subdominio</li>
        <li>10 BD</li>
        <li>Softaculous: Instale Wordpress, Moodle, Joomla, Drupal y otras 300 aplicaciones con un solo click</li>
      `,
        Head: false,
      },
      {
        text: `
        <li>50 GB de Espacio NVMe</li>
        <li>Transferencia Ilimitada</li>
        <li>6 dominios</li>
        <li>15 subdominio</li>
        <li>15 BD</li>
        <li>Softaculous: Instale Wordpress, Moodle, Joomla, Drupal y otras 300 aplicaciones con un solo click</li>
        `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "", Head: false, More:true},
     
    ],
  },
  {
    fila: [
      { text: "DESARROLLO", Head: true },
      {
        text: `
        <li>PHP, Python, NodeJS</li>
        <li>Acceso SSH</li>
        <li>Acceso Git</li>
        <li>MemCache</li>
        
      `,
        Head: false,
      },
      {
        text: `
        <li>PHP, Python, NodeJS</li>
        <li>Acceso SSH</li>
        <li>Acceso Git</li>
        <li>MemCache</li>
      `,
        Head: false,
      },
      {
        text: `
      <li>PHP, Python, NodeJS</li>
      <li>Acceso SSH</li>
      <li>Acceso Git</li>
      <li>MemCache</li>
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "CORREOS", Head: true },
      {
        text: `
        <li>100 correos</li>
        <li>MapachioBox Web</li>
        <li>MapachioBox APP</li>
        <li>RoundCube</li>
        <li>Envi&oacute; y Recepci&oacute;n Proactiva de Mail: Servicio Mejorado de Envi&oacute;</li>
        `,
        Head: false,
      },
      {
        text: `
        <li>100 correos</li>
        <li>MapachioBox Web</li>
        <li>MapachioBox APP</li>
        <li>RoundCube</li>
        <li>Envi&oacute; y Recepci&oacute;n Proactiva de Mail: Servicio Mejorado de Envi&oacute;</li>
      `,
        Head: false,
      },
      {
        text: `
        <li>100 correos</li>
        <li>MapachioBox Web</li>
        <li>MapachioBox APP</li>
        <li>RoundCube</li>
        <li>Envi&oacute; y Recepci&oacute;n Proactiva de Mail: Servicio Mejorado de Envi&oacute;</li>
      
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: `OPTIMIZACI&oacute;N`, Head: true },
      {
        text: `
      <li>100%  + Velocidad Servidor Web (LiteSpeed).</li>
      <li>LS Cache: Mejora m&aacute;s la velocidad de tu web en Wordpress al 100% gratis.</li>
      <li>200% + Velocidad de Procesador que  planes b&aacute;sico.</li>
      <li>Discos NVME (3600% m&aacute;s veloz vs Hosting Normales)</li>
      <li>8MB I/O Extendido</li>
      <li>8RAM</li>
        
      `,
        Head: false,
      },
      {
        text: `
      <li>100%  + Velocidad Servidor Web (LiteSpeed).</li>
      <li>LS Cache: Mejora m&aacute;s la velocidad de tu web en Wordpress al 100% gratis.</li>
      <li>200% + Velocidad de Procesador que  planes b&aacute;sico.</li>
      <li>Discos NVME (3600% m&aacute;s veloz vs Hosting Normales)</li>
      <li>16MB I/O Extendido</li>
      <li>16RAM</li>
      `,
        Head: false,
      },
      {
        text: `
      <li>100%  + Velocidad Servidor Web (LiteSpeed).</li>
      <li>LS Cache: Mejora m&aacute;s la velocidad de tu web en Wordpress al 100% gratis.</li>
      <li>200% + Velocidad de Procesador que  planes b&aacute;sico.</li>
      <li>Discos NVME (3600% m&aacute;s veloz vs Hosting Normales)</li>
      <li>32MB I/O Extendido</li>
      <li>32RAM</li>
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "SEGURIDAD", Head: true },
      {
        text: `
        <li>SSL Gratutito para todos los dominios</li>
        <li>Antivirus</li>
        <li>Firewall</li>
        <li>Protecci&oacute;n contra Ataques DDOS</li>
        <li>Seguridad por Reglas.</li>
        <li>Backup Diario.</li>
        
      `,
        Head: false,
      },
      {
        text: `
        <li>SSL Gratutito para todos los dominios</li>
        <li>Antivirus</li>
        <li>Firewall</li>
        <li>Protecci&oacute;n contra Ataques DDOS</li>
        <li>Seguridad por Reglas.</li>
        <li>Backup Diario.</li>
        
      `,
        Head: false,
      },
      {
        text: `
        <li>SSL Gratutito para todos los dominios</li>
        <li>Antivirus</li>
        <li>Firewall</li>
        <li>Protecci&oacute;n contra Ataques DDOS</li>
        <li>Seguridad por Reglas.</li>
        <li>Backup Diario.</li>
        
      `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "Soporte", Head: true },
      { text: `Soporte Profesional por Whatsapp.`, Head: false },
      { text: `Soporte Profesional por Whatsapp.`, Head: false },
      { text: `Soporte Profesional por Whatsapp.`, Head: false },
    ],
  },
  {
    fila: [
      { text: "Inversi&oacute;n anual", Head: true },
      { text: DataPlanDesarrollo[0].precio, Head: false },
      { text: DataPlanDesarrollo[1].precio, Head: false },
      { text: DataPlanDesarrollo[2].precio, Head: false },
    ],
  },
  {
    fila: [
      { text: "Inversi&oacute;n mensual", Head: true },
      { text: "$27.08", Head: false },
      { text: "$37.50", Head: false },
      { text: "$58.33", Head: false },
    ],
  },
  {
    fila: [
      { text: "Addons", Head: true },
      {
        text: `
        <li>25 Correos Adicionales $10.00</li>
        <li>1 Dominio Adicional $50.00</li>
        <li>1 Subdominio $25.00</li>
        `,
        Head: false,
      },
      {
        text: `
        <li>25 Correos Adicionales $10.00</li>
        <li>1 Dominio Adicional $50.00</li>
        <li>1 Subdominio $25.00</li>
        `,
        Head: false,
      },
      {
        text: `
        <li>25 Correos Adicionales $10.00</li>
        <li>1 Dominio Adicional $50.00</li>
        <li>1 Subdominio $25.00</li>
        `,
        Head: false,
      },
    ],
  },
  {
    fila: [
      { text: "", Head: false },
      {
        text: "Comprar",
        Head: false,
        Btn: true,
        url: Mensaje(DataPaquetes[6].title, DataPlanDesarrollo[0].title),
      },
      {
        text: "Comprar",
        Head: false,
        Btn: true,
        url: Mensaje(DataPaquetes[6].title, DataPlanDesarrollo[1].title),
      },
      {
        text: "Comprar",
        Head: false,
        Btn: true,
        url: Mensaje(DataPaquetes[6].title, DataPlanDesarrollo[2].title),
      },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[2].title) },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[].title },
      // { text: "Comprar", Head: false, Btn: true, url: Mensaje(DataPaquetes[0].title, DataPlanInicio[].title },
    ],
  },
]
export const DataPrecio = [
  {
    title: DataPaquetes[0].title,
    url:DataPaquetes[0].url,
    details: DataPrecioInicio,
  },
  {
    title: DataPaquetes[1].title,
    url:DataPaquetes[1].url,
    details: DataPrecioGestor,
  },
  {
    title: DataPaquetes[2].title,
    url:DataPaquetes[2].url,
    details: DataPrecioEmpresa,
  },
  {
    title: DataPaquetes[3].title,
    url:DataPaquetes[3].url,
    details: DataPrecioCorporativo,
  },
  {
    title: DataPaquetes[4].title,
    url:DataPaquetes[4].url,
    details: DataPrecioVPS,
  },
  {
    title: DataPaquetes[5].title,
    url:DataPaquetes[5].url,
    details: DataPrecioEducacion,
  },
  {
    title: DataPaquetes[6].title,
    url:DataPaquetes[6].url,
    details: DataPrecioDesarrollo,
  },
]
