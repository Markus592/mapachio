export const TelefonoWhatsapp='+51933138573'
export const MensajeGeneral=`Hola%20Mapachio%20me%20gustaria%20saber%20mas%20sobre%20sus%20servicios`
export const MensajeSoporte=`Hola Mapachio me gustaria que me ayuden con el soporte`
export const ApiWhatsapp=`https://api.whatsapp.com/send?phone=${TelefonoWhatsapp}&text=`
export const MensajeWhatsappGeneral=`${ApiWhatsapp}${MensajeGeneral}`
export const Pyme ='Pyme'
export const Empresa ='Empresa'
export const Corporacion ='Corporación'
export const Estatal ='Estatal'
export const Apps ='Apps'  