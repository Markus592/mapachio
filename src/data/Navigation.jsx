export const Navigation = [
  { name: "Inicio", url: `/` },
  { name: "Nosotros", url: `/nosotros` },
  { name: "Tecnología", url: `/tecnologia` },
  { name: "Precios", url: `/precios` },
  { name: "Blog", url: `/blog` },
  { name: "Contacto", url: `/contacto` },
]
