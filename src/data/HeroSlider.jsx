import Ardilla from "../assets/images/ardilla.png"
import Buho from "../assets/images/buho.png"
import Liebre from "../assets/images/liebre.png"
import Mapache from "../assets/images/mapache.png"
import Heromapache from "../assets/images/Heromapache.png"
import Capitanmapachio from "../assets/images/capitanmapachio.png"
import { ApiWhatsapp, MensajeSoporte } from "./variablesConstantes"

export const DataHeroSlider = [
  {
    title: `Dale Cari&ntilde;o a tu Web`,
    text: `
        Con nuestro conocimiento
        ancestral en tecnolog&iacute;as de
        servidores.`,
    Url: "/blog/como-damos-carino-a-tu-web/",
    TextLink: `Conoce nuestro Cari&ntilde;o`,
    img: Heromapache,
  },
  {
    title: `Correos que enamoran`,
    text: `
        Tus correos <u><b>SIEMPRE</b></u> llegaran al
        Inbox, olv&iacute;date del spam.`,
    Url: "/blog/correos-que-enamoran/",
    TextLink: `Lo importante de la comunicaci&oacute;n`,
    img: Mapache,
  },
  {
    title: `Velocidad de Combate`,
    text: `
        Tecnolog&iacute;as &uacute;nicas que llevaran
        tu web a nuevas realidades.`,
    Url: "/blog/como-es-que-mejoramos-la-velocidad-de-tu-web/",
    TextLink: `Conoce nuestra receta secreta de velocidad`,
    img: Liebre,
  },
  {
    title: `Seguridad Extrema`,
    text: `
        Protegemos tu web y a tus clientes de
todo tipo de vicisitudes digitales.`,
    Url: "/blog/seguridad-extrema/",
    TextLink: `¿A que se refieren con extremo?`,
    img: Buho,
  },
  {
    title: `Cuidamos el Mundo
    `,
    text: `
    Con nuestros servidores basados
    en energ&iacute;a renovable t&uacute; tambi&eacute;n
    lo cuidaras`,
    Url: "/blog/protegemos-el-medio-ambiente/",
    TextLink: `Capit&aacute;n Mapachio Approved
    `,
    img: Capitanmapachio,
  },
  {
    title: `Siempre Listos`,
    text: `
    Obt&eacute;n Paz en tu negocio con nuestro 
    soporte activo.`,
    Url: `${ApiWhatsapp}${MensajeSoporte}`,
    TextLink: `¿Necesitas Paz y Soporte?`,
    WhatLink:true,
    img: Ardilla,

  },
]
