export const DataPlanInicio = [
  {
    title: "Starter 250MB",
    velocidad: "250MB",
    precio: "$ 18.00",
    letra: "S1",
    text: "Ideal para P&aacute;ginas Webs Simples con HTML", 
    textAlt: "Ideal para P&aacute;ginas Webs Simples con HTML", 
  },
  {
    title: "Starter 500MB",
    velocidad: "500MB",
    precio: "$ 28.00",
    letra: "S2",
    text:"P&aacute;ginas de tipo landing Pages con alg&uacute;n gestor de contenidos simple.",
    textAlt:"P&aacute;ginas de tipo landing Pages con alg&uacute;n gestor de contenidos simple.",
  },
  {
    title: "Starter 1GB",
    velocidad: "1GB",
    precio: "$ 39.00",
    letra: "S3",
    text: "Ideal para Pequeñas Empresas con 5 trabajadores", 
    textAlt: "Ideal para Pequeñas Empresas con 5 trabajadores", 
  },
  
]
export const DataPlanGestor = [
  {
    title: "Gestor Wordpress 2.5GB",
    velocidad: "2.5GB",
    precio: "$ 69.00",
    letra: "G1",
    text:"Si tienes un Wordpress con m&aacute;s de 5 plugins o una web con 5 secciones esta es tu opci&oacute;n ideal. Incluye nuestro poderoso sistema MapachioBox para un manejo amigable de los correos electr&oacute;nicos.",
    textAlt:"Si tienes un Wordpress con m&aacute;s de 5 plugins o una web con 5 secciones esta es tu opci&oacute;n ideal.",
    
  },
  {
    title: "Gestor Drupal 5GB",
    velocidad: "5GB",
    precio: "$ 85.00",
    letra: "G2",
    text:"Si Tienes un Wordpress con una buena cantidad de plugins o una web en  Drupal  La velocidad es mejorada para que pueda correr sin problemas.",
    textAlt:"Si Tienes un Wordpress con una buena cantidad de plugins o una web en  Drupal.",
  },
  {
    title: "Tienda Online 10GB",
    velocidad: "10GB",
    precio: "$ 119.00",
    letra: "G3",
    text:"Si Tienes un Wordpress con una buena cantidad de plugins o una web en  Drupal  La velocidad es mejorada para que pueda correr sin problemas.",
    textAlt:"Si Tienes un Wordpress con una buena cantidad de plugins o una web en  Drupal.",
  },
  
]
//COMO VARIABLE=> DataplanGESTOR[0].TITLE
export const DataPlanEmpresa = [
  {
    title: "PYME 25GB",
    velocidad: "25GB",
    precio: "$ 165.00",
    letra: "P1",
    text:"Ideal para Instituciones Estatales o Empresas Medianas con 50 trabajadores. Empresas de Diseño Grafico que recien empiezan y necesitan enviar artes a sus clientes sin viscisitudes.",
    textAlt:"Ideal para Instituciones Estatales o Empresas Medianas con 50 trabajadores.",
  },
  {
    title: "PYME 50GB",
    velocidad: "50GB",
    precio: "$ 300.00",
    letra: "P2",
    text:"Ideal para Instituciones Estatales o Empresas Medianas con 100 trabajadores. Empresas Constructoras pequeñas que env&iacute;an planos de construcci&oacute;n por correo.",
    textAlt:"Ideal para Instituciones Estatales o Empresas Medianas con 100 trabajadores.",
  },
  {
    title: "PYME 100GB",
    velocidad: "100GB",
    precio: "$ 550.00",
    letra: "P3",
    text:"Ideal para Instituciones Estatales o Empresas Medianas con 200 trabajadores",
    textAlt:"Ideal para Instituciones Estatales o Empresas Medianas con 200 trabajadores",
  },
  
]
export const DataPlanCorporativo = [
  {
    title: "Corporativo 200GB",
    velocidad: "200GB",
    precio: "$ 1000.00",
    letra: "C1",
    text: `Empresas Grandes o Instituciones del Estado con m&aacute;s de 100 personas. Constructoras o Empresas de Arquitectura con m&aacute;s de 3 años de experiencia que env&iacute;an continuamente planos pesados a sus clientes.`,
    textAlt: `Empresas Grandes o Instituciones del Estado con m&aacute;s de 100 personas.`,
  },
  {
    title: "Corporativo 400GB",
    velocidad: "400GB",
    precio: "$ 1800.00",
    letra: "C2",
    text: `Organismos con m&aacute;s de 200 personas que necesitan enviar m&aacute;s de 400 correos por d&iacute;a. `,
    textAlt: `Organismos con m&aacute;s de 200 personas que necesitan enviar m&aacute;s de 400 correos por d&iacute;a. `,
  },
  
  
]
export const DataPlanVPS = [
  {
    title: "VPS 800GB",
    velocidad: "800GB",
    precio: "$ 3200.00",
    letra: "V3",
    text:"Organismos con m&aacute;s de 400 personas con env&iacute;o de m&aacute;s de 800 correos al d&iacute;a",
    textAlt:"Organismos con m&aacute;s de 400 personas con env&iacute;o de m&aacute;s de 800 correos al d&iacute;a",
  },
  {
    title: "VPS 1.6TB",
    velocidad: "1.6TB",
    precio: "$ 6000.00",
    letra: "V1",
    text:"Empresas que requieran brindar el servicio de Hosting y Dominio en Per&uacute; y deseen bajar sus costos tributarios. Todos los accesos ilimitados",
    textAlt:"Empresas que requieran brindar el servicio de Hosting y Dominio en Per&uacute;.",
  },
  {
    title: "VPS 2.4TB",
    velocidad: "2.4TB",
    precio: "$ 10000.00",
    letra: "V2",
    text:"Empresas que requieran brindar el servicio de Hosting y Dominio en Per&uacute; y deseen bajar sus costos tributarios.Todos los accesos ilimitados",
    textAlt:"Empresas que requieran brindar el servicio de Hosting y Dominio en Per&uacute;.",
  },
  
  
]
export const DataPlanEducacion = [
  {
    title: "LMS 5GB",
    velocidad: "5GB",
    precio: "$ 150.00",
    letra: "L1",
    text:"Instituciones Educativas que recien empiezan y que cuenten con una web y un sistema de Elearning con hasta 25 personas conectadas a su Elearning",
    textAlt:"Instituciones Educativas que recien empiezan y que cuenten con una web.",
  },
  {
    title: "LMS 25GB",
    velocidad: "25GB",
    precio: "$ 250.00",
    letra: "L2",
    text:"Instituciones Educativas con mas de 5 cursos virtuales. Util para 35 personas al mismo tiempo.",
    textAlt:"Instituciones Educativas con mas de 5 cursos virtuales.",
  }
  ,
  {
    title: "LMS 50GB",
    velocidad: "50GB",
    precio: "$ 350.00",
    letra: "L3",
    text:"Instituciones Educativas con mas de 10 cursos virtuales que necesiten tener 50 personas conectadas al mismo tiempo dando un ex&aacute;men por ejemplo",
    textAlt:"Instituciones Educativas con mas de 10 cursos virtuales.",
  }
  
]
export const DataPlanDesarrollo = [
  {
    title: "APP WEB 5GB",
    velocidad: "5GB",
    precio: "$ 260.00",
    letra: "A1",
    text:"Servidor para probar aplicaciones web en una instancia de prueba. Viene con herramientas como Acceso a GIT para que puedas trabajar en equipo. Tambien acceso a SSH para instalar mejoras como Compser, Drush ",
    textAlt:"Servidor para probar aplicaciones web en una instancia de prueba.",
  },
  {
    title: "APP WEB 25GB",
    velocidad: "25GB",
    precio: "$ 360.00",
    letra: "A2",
    text:"Servidor para deployar aplicaciones web. Viene con herramientas como Acceso a GIT para que puedas trabajar en equipo. Tambien acceso a SSH para instalar mejoras como Compser, Drush o Scripts generales en tu servidor.  Podras tener alrededor de 20 personas conectadas usando tu aplicaci&oacute;n.",
    textAlt:"Servidor para deployar aplicaciones web.",
  }
  ,
  {
    title: "APP WEB 50GB",
    velocidad: "50GB",
    precio: "$ 560.00",
    letra: "A3",
    text:"Servidor de Produccion con todas las herramientas necesarias para facilitar el desarrollo de tu software y manejar el trabajo en Equipo. Podras tener un promedio de 50 personas al mismo tiempo usando tu aplicaci&oacute;n con la capacidad de I/O entendida que brindamos.",
    textAlt:"Servidor de Produccion con todas las herramientas necesarias para facilitar el desarrollo de tu software y manejar el trabajo en Equipo.",
  }
]
export const DataPlanesMigracion=[
  {
    title: "Migrar mi hosting con ustedes",
    velocidad: "",
    precio: "",
    letra: "M1",
    text:"",
    textAlt:"",
  }

]
export const DataPlanesDomain = [
  { title: ".com", precio: "$18.00", ejemplo: "minegocio.com" },
  { title: ".pe", precio: "$39.00", ejemplo: "minegocio.pe" },
  { title: "otros", precio: "", ejemplo: "otro tipo de dominio" },
  { title: "Migraci&oacute;n", precio: "", ejemplo: "" },
]
export const DataPaquetes = [

  {
    title: "INICIO",
    text:
      "Paquete mas Basico Posible. Util para Webs en HTML o con poca capacidad de calculo.",
    plan:DataPlanInicio,
    url:'/precio/inicio'

  },
  {
    title: "GESTOR DE CONTENIDOS",
    text:
    "Ideal para Gestores de Contenidos como Wordpress, Drupal, Joomla y Tiendas Virtuales como Magento y PrestaShop",
    plan:DataPlanGestor,
    url:'/precio/gestor-de-contenidos'
  },
  {
    title: "EMPRESAS Y GOBIERNO",
    text:
    "No solo buscas potencia en tu p&aacute;gina web si no el manejo de correos electr&oacute;nicos de una forma efectiva. Ideal para empresas con mas de 20 personas y entidades estatales",
    plan:DataPlanEmpresa,
    url:'/precio/empresas-y-gobierno'
  },
  {
    title: "CORPORATIVO",
    text:
    "Servidores Masivos para Medianas y Grandes Empresas que requieran de una buena capacidad de espacio en su hosting.",
    plan:DataPlanCorporativo,
    url:'/precio/corporativo'
  },
  {
    title: "VPS",
    text:
    "Para empresas proveedoras de Hostings o Revendedores",
    plan:DataPlanVPS,
    url:'/precio/vps'
  },
  {
    title: "EDUCACION",
    text:
    "¿Tu p&aacute;gina web contara con mas de 20 usuarios conectados al mismo tiempo rindiendo un examen? Este es tu opci&oacute;n de Hosting",
    plan:DataPlanEducacion,
    url:'/precio/educacion'
  },
  {
    title: "DESARROLLO",
    text:
    "Hostings enfocados para desarrollo con todas las herramientas necesarias para que puedan subir y actualizar su  proyecto. Viene con diferentes lenguajes de Programaci&oacute;n",
    plan:DataPlanDesarrollo,
    url:'/precio/desarrollo'
  },
  {
    title: "MIGRACI&oacute;N Y OTROS",
    text:
    "Deseo migrar mi hosting con ustedes.",
    plan:DataPlanesMigracion
    // plan:DataPlanDesarrollo
  },
]
