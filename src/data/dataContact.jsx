import { ApiWhatsapp } from "./variablesConstantes";

export const Comercial=[
    {
        title:`Per&uacute;`,
        text:`Ventas en Per&uacute;`,
        email:`peru@mapachio.com`,
        Url:`${ApiWhatsapp}Hola mapachio me gustaria contactarme con Per&uacute;`
    },
    
    {
        title:`Reino Unido`,
        text:`Ventas en UK`,
        email:`uk@mapachio.com`,
        Url:`https://api.whatsapp.com/send?phone=+447983091518&text=Hola mapachio me gustaria contactarme con Reino Unido`
    },
]
export const ComercialInternacional=[
    {
        title:`España`,
        text:`Ventas en España`,
        email:`espana@mapachio.com`,
        Url:`https://api.whatsapp.com/send?phone=+34657628676&text=Hola mapachio me gustaria contactarme con España`
    },
    {
        title:`Ecuador`,
        text:`Ventas en Ecuador`,
        email:`ecuador@mapachio.com`,
        Url:`https://api.whatsapp.com/send?phone=+593988382233&text=Hola mapachio me gustaria contactarme con España`
    },
    {
        title:`Uruguay`,
        text:`Ventas en Uruguay`,
        email:`uruguay@mapachio.com`,
        Url:`https://api.whatsapp.com/send?phone=+447983091518&text=Hola mapachio me gustaria contactarme con Uruguay`,
        NotBtn:true
    },
]
export const Soporte =[
    {
        title:`Soporte Técnico Español`,
        text:`Soporte Técnico Internacional`,
        email:`soporte@mapachio.com`,
        Url:`${ApiWhatsapp}Hola mapachio me gustaria que me brinden soporte`
    },
    {
        title:`Tech Support English`,
        text:`International Tech Support English`,
        email:`support@mapachio.com`,
        Url:`${ApiWhatsapp}Hola mapachio me gustaria que me brinden soporte`
    },
]