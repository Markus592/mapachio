import React from "react"
import { Link } from "gatsby"
import Layout from "../components/Layout"
// import Hero from "../components/Hero"
import BloqueTabsPrecio from "../components/Precio/BloqueTabsPrecio"
import Tableprecio from "../components/Precio/TablePrecio"
import tw from "twin.macro"
import styled from "@emotion/styled"
import { DataPrecio, DataPrecioInicio } from "../data/DataPrecio"
import Banner from "../components/banner"
import ImageOg from "../assets/images/P3Og.png"
import SEO from "../components/Seo"
export const Container = styled.div`
  ${tw`bg-azul `};
`
const Precios = () => {
  return (
    <Layout>
      {/* <Hero> </Hero> */}
      <SEO
        title="Precios"
        description="Consulta todos nuestros paquetes y elige el que te lleve a la victoria."
        titleOg=" Precios y Paquetes"
        descriptionOg="Asi como todos tienen una media naranja en Mapachio Host hemos creado un paquete ideal para tus necesidades en Hosting y Dominios."
        image={ImageOg}
      ></SEO>
      <Banner title="Precios" alt="precios"></Banner>
      <Container>
        <BloqueTabsPrecio
          as={Link}
          data={DataPrecio}
          indice={0}
        ></BloqueTabsPrecio>
        <Tableprecio data={DataPrecioInicio}></Tableprecio>
        {/* <Tableprecio data={DataPrecioPyme}></Tableprecio> */}
      </Container>
      {/* <h1>Hi people</h1> */}
      {/* <Button>Activate</Button> */}
    </Layout>
  )
}

export default Precios
