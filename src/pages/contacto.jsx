import React from "react"
import Banner from "../components/banner"
import Block from "../components/Contact/Block"
import Layout from "../components/Layout"
import SEO from "../components/Seo"
import { Comercial } from "../data/dataContact"
import { ComercialInternacional } from "../data/dataContact"
import { Soporte } from "../data/dataContact"
import ImageOg from "../assets/images/ardillaOg.png"

export default function Contact() {
  return (
    <Layout>
      <SEO
        title="Contacto"
        description="Olvídate del formulario de contacto. Consulta directamente a través de Whatsapp todos nuestros beneficios y paquetes. Atendemos a todo el Planeta en Español e Ingles.
      "
        titleOg="Contacto"
        descriptionOg="Trabajamos en todo el Planeta y tenemos representantes en España, Uruguay, Perú, Ecuador y Reino Unido."
        image={ImageOg}
      ></SEO>
      <Banner title="Contacto" alt="contacto"></Banner>
      <Block data={Comercial} title="Comercial"></Block>
      <Block
        data={ComercialInternacional}
        title="Oficinas Internacionales"
      ></Block>
      <Block data={Soporte} title="Soporte Técnico"></Block>
    </Layout>
  )
}
