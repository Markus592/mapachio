import React from "react"
import Layout from "../components/Layout"
import Hero from "../components/Hero"
// import tw, { styled } from "twin.macro"
import BlockPaquetes from "../components/Inicio/BlockPaquetes"
// import BlockPlanes from "../components/Inicio/BlockPlanes"
import PlanesDomain from "../components/Inicio/domain/PlanesDomain"
import Questions from "../components/Inicio/Questions"
import Blog from "../components/Inicio/BloqueBlog"
import SEO from "../components/Seo"
import ImageOg from "../assets/images/Heromapache.png"
// const Button = styled.button`
//   ${tw`bg-azulino  text-white p-2 rounded`}
// `
export default function Home() {
  return (
    <Layout>
      <SEO
        noTemplate={true}
        title="Mapachio Hosting Reforzado  - Servicio de Hostings y dominios mundiales. Le damos cariño a tu web."
        description='Conoce Mapachio, la nueva era en servicios de Hostings y Dominios. No somos una empresa común y corriente, nos preocupamos por darte las mejores tecnologías para cumplir nuestro objetivo final "Darle Cariño a Tu Web" '
        titleOg="Dale Cariño a tu Web"
        descriptionOg="Hola, somos Mapachio Hosting Reforzado y nuestro objetivo es darle cariño a tu web. Tenemos mas de 5 años de experiencia fortaleciendo nuestros conocimientos en optimización de servidor y seguimos mejorando, y no es un cliché, en verdad lo hacemos. "
        image={ImageOg}
      ></SEO>
      <Hero> </Hero>
      <BlockPaquetes></BlockPaquetes>
      {/* <BlockPlanes></BlockPlanes> */}
      {/* <PlanesDomain></PlanesDomain> */}
      <Questions></Questions>
      <Blog></Blog>
      {/* <h1>Hi people</h1> */}
      {/* <Button>Activate</Button> */}
    </Layout>
  )
}
