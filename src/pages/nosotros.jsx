import React from "react"
import Layout from "../components/Layout"
import Banner from "../components/banner"
import tw from "twin.macro"
import styled from "@emotion/styled"
import Mapachio from "../assets/images/mapachioNosotros.png"
import { CardNosotros } from "../components/StyleNosotros"
import SEO from "../components/Seo"
import ImageOg from '../assets/images/mapachioNosotrosOg.png'
const Container = styled.div`
  ${tw`
    grid
    py-10
    
    `};
  grid-template-columns: auto 1fr;
  img {
    align-self: center;
    width: 100%;
  }
  @media (max-width: 800px) {
    grid-template-columns: 1fr;
    img {
      width: 50%;
      margin: 2rem auto 0;
    }
  }
`
const WrapperText = styled.div`
  ${tw`
    text-azul
    max-w-3xl
    text-xl
 `};
`
export default function Nosotros() {
  return (
    <Layout>
      <SEO
      title='Nosotros'
      description='Conoce todo Acerca de la Misión de Mapachio en el Mundo'
        titleOg="Conoce nuestra empresa y nuestro pensamiento."
        descriptionOg="Una presentación cariñosa de lo que es Mapachio Host. Somos mucho mas que un hosting de calidad."
        image={ImageOg}
      ></SEO>
      <Banner title="Nosotros" alt="Nosotros"></Banner>
      <Container className="container">
        <WrapperText>
          Somos una empresa digital descentralizada con bases en diferentes
          países del mundo.
          <br />
          No somos una empresa normal del tipo Brick & Mortar, ni queremos
          serlo, los tiempos cambian y la libertad de ser un ciudadano mundial
          nos atrae demasiado.
          <br />
          Nuestra oficina administrativa es cualquier parte del Mundo en donde
          se encuentre su fundador y nuestro equipo Administrativo, aunque
          Arequipa, Perú es donde pasamos más nuestro tiempo. Es una ciudad
          hermosa, ¡vengan a visitarla!
          <br />
          Contamos con representación legal y comercial en Ecuador, Uruguay,
          España y Reino Unido. Y si quieres formar parte de nuestro equipo
          mándanos un mensaje.
          <br />
          Nuestro soporte técnico de primera línea se encuentra permanentemente
          conectado en Lima, Perú. Nuestros técnicos de segunda a línea se
          encuentran en Cochin, India y el soporte TIER-3 en caso de un
          apocalipsis zombie se encuentra en Falkenstein, Alemania
        </WrapperText>

        <img src={Mapachio} alt="Nosotros Mapachio" />
      </Container>
      <CardNosotros>
        <div className="container ">
          <p>
            ¿Misión? ¿Visión? no gracias preferimos darte a conocer nuestro
            Propósito Masivo Transformativo
          </p>
          <h3>Darle Cariño a tu Web </h3>
        </div>
      </CardNosotros>
      <WrapperText className="container wrapper">
        Y cariño tendrás si contratas nuestros servicios. Sabemos lo que es
        tener un servicio de calidad, costo efectivo y con soporte permanente.
        <br />
        Sabemos además que no sonamos como las otras empresas de Hostings, es
        porque no lo somos y no queremos serlo.
        <br />
        Nuestro propósito se basa en la idea de Libertad para todos, libertad de
        presiones a nuestros clientes dándoles las menores preocupaciones
        posibles y libertad también a nuestros colaboradores de poder investigar
        nuevas tecnologías y métodos que beneficien a todos
      </WrapperText>
      <CardNosotros>
        <div className="container">
          <p className="text-white">
            Nuestra empresa en voluntad de su fundador es ser un actor benigno
            en el mundo. Y lo vamos a lograr con el apoyo de todos
          </p>
        </div>
      </CardNosotros>
    </Layout>
  )
}
