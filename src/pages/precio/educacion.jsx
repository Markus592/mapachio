import React from "react"
import Layout from "../../components/Layout"
// import Hero from "../components/Hero"
import BloqueTabsPrecio from "../../components/Precio/BloqueTabsPrecio"
import Tableprecio from "../../components/Precio/TablePrecio"
import {Title} from "../../components/Title"
import tw from "twin.macro"
import styled from "@emotion/styled"
import { DataPrecio, DataPrecioEducacion } from "../../data/DataPrecio"
import Banner from "../../components/banner"
import { Link } from "gatsby"
import SEO from "../../components/Seo"
import ImageOg from "../../assets/images/P3Og.png"
export const Container = styled.div`
  ${tw`bg-azul `};
`
const Educacion = () => {
  return (
    <Layout>
      {/* <Hero> </Hero> */}
      <SEO
        title="Precio Educacion"
        description="Consulta todos nuestros paquetes y elige el que te lleve a la victoria."
        titleOg=" Precio Educacion"
        descriptionOg="Asi como todos tienen una media naranja en Mapachio Host hemos creado un paquete ideal para tus necesidades en Hosting y Dominios."
        image={ImageOg}
      ></SEO>
      <Banner title="Precios" alt="precios"></Banner>
      <Container>
        <Title rosado={true}>{DataPrecio[5].title}</Title>
        <BloqueTabsPrecio
          as={Link}
          data={DataPrecio}
          indice={5}
        ></BloqueTabsPrecio>
        <Tableprecio data={DataPrecioEducacion}></Tableprecio>
      </Container>
      {/* <h1>Hi people</h1> */}
      {/* <Button>Activate</Button> */}
    </Layout>
  )
}

export default Educacion
