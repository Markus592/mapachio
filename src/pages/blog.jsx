import React from "react"
import Layout from "../components/Layout"
import Banner from "../components/banner"
import BlockBlogCard from "../components/Blog/BlockBlogCard"
import SEO from "../components/Seo"
import ImageOg from "../assets/images/P4Og.png"
export default function Blog() {
  return (
    <Layout>
      <SEO
        title="Blog"
        description="Escribimos desde el corazón. Varios artículos que te llevaran lejos."
        titleOg=" Blog"
        descriptionOg=" Descubre todo sobre Hostings y Dominios en nuestro cariñoso blog."
        image={ImageOg}
      ></SEO>

      <Banner title="Artículos De Blog" alt="Artículos de blog"></Banner>
      <BlockBlogCard></BlockBlogCard>
    </Layout>
  )
}
