import React from "react"
import Layout from "../components/Layout"
import { BlockTech, Item } from "../components/Tecnologia/BlockCard"
import { Title } from "../components/Title"
import SEO from "../components/Seo"
import Banner from "../components/banner"
import img1 from "../assets/images/tech1.jpg"
import img2 from "../assets/images/tech2.jpg"
import img3 from "../assets/images/tech3.jpg"
import img4 from "../assets/images/tech4.jpg"
import img5 from "../assets/images/tech5.jpg"
import img6 from "../assets/images/tech6.jpg"
import ImageOg from "../assets/images/liebreOg.png"
export default function Tecnologia() {
  return (
    <Layout>
      <SEO
        title="Tecnologia"
        description="En Mapachio usamos una combinación única de tecnologías para darle ese toquecito de cariño extra a tu web. Conoce nuestro abanico tecnológico."
        titleOg="Conoce nuestras tecnologías"
        descriptionOg="Tecnología y Cariño es Mapachio Host. Descubre que tecnologías estamos usando para ser los mejores."
        image={ImageOg}
      ></SEO>
      <Banner white={true} title="Tecnología"></Banner>
      <BlockTech>
        <Title rosado={true}>Cpanel y Softaculous</Title>
        <Item>
          <img
            src={img1}
            alt="Cpanel y Softaculous
"
          />
          <p>
            El Panel de Control más popular del mercado es una parte de nuestro
            cariño hacia ustedes, además de que contamos con el paquete de
            instalación de aplicaciones mas útil. ¡Instala Wordpress, Drupal o
            tu programa favorito con un click!
          </p>
        </Item>
      </BlockTech>
      <BlockTech>
        <Title rosado={true}>Servidores 2 veces más rapido</Title>
        <Item bgBlanco={true}>
          <img
            src={img2}
            alt="Servidores 2 veces mas rapido
"
          />
          <p>
            En Mapachio usamos LiteSpeed que nos permite mejorar la velocidad de
            entrega entre 2 a 3 veces más rápido que un servidor Apache, lo más
            común en Hostings que no dan cariño
          </p>
        </Item>
      </BlockTech>
      <BlockTech>
        <Title rosado={true}>Correos Corporativos Asombrosos</Title>
        <Item>
          <img
            src={img3}
            alt="Correos Corporativos Funcionales
"
          />
          <p>
            Gracias a nuestra tecnología de Mail Relay los correos llegan si o
            si a la carpeta de Inbox de tus clientes. Además, contamos con
            nuestra tecnología de filtrado de Spam para ayudarte a hacer tu día
            más agradable, sin Spam.
          </p>
        </Item>
      </BlockTech>
      <BlockTech>
        <Title rosado={true}>Gestos de Correos MachioBox</Title>
        <Item bgBlanco={true}>
          <img
            src={img4}
            alt="Gestos de Correos MachioBox
"
          />
          <p>
            Si quieres un GMAIL Corporativo, pero aún no estas listo para la
            inversión, te ofrecemos a Machiobox un sistema gestor de correos con
            Contactos, Firmas y sistema de almacenamiento de archivos, ¡Como
            Google Drive!.
            <br />
            Gratis en nuestras cuentas de 2.5GB para arriba.
          </p>
        </Item>
      </BlockTech>
      <BlockTech>
        <Title rosado={true}>Seguridad y Protección</Title>
        <Item>
          <img
            src={img5}
            alt="Seguridad y Proteccion
"
          />
          <p>
            Contamos con 4 capas de protección para tu servidor. Nuestro Muro de
            Fuego para impedir ataques comunes, nuestro sistema de protección
            contra ataques de Fuerza Bruta, nuestro analizador de ataques por
            reglas y nuestro antivirus para los archivos de tu web.
            <br />
            Adicionalmente sacamos copias de seguridad periódicas de toda tu
            cuenta, para que en casos extremos sigas siempre online.
          </p>
        </Item>
      </BlockTech>
      <BlockTech>
        <Title rosado={true}>Servidores con Energias Renovables</Title>
        <Item bgBlanco={true}>
          <img
            src={img6}
            alt="Servidores con Energias Renovables
"
          />
          <p>
            No solo damos cariño a tu web si no al planeta. Nuestros servidores
            utilizan Poder Hídrico y del Viento para energizarse. Además, que
            usamos hardware eficiente para el control del consumo eléctrico.
          </p>
        </Item>
      </BlockTech>
    </Layout>
  )
}
